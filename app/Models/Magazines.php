<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
class Magazines extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'magazines';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mag_profile_pic', 'mag_pdf','mag_date','id'];

     public static function removeMagazine($id)
    {
        $file = Magazines::findOrFail($id);
         
        if ($file->mag_profile_pic != '') {

            File::delete('theme/uploads/magazines/' . $file->mag_profile_pic);
            File::delete('theme/uploads/magazines/' . $file->mag_pdf);
        }
        Magazines::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('theme/uploads/magazines/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = str_replace(" ","",$inputname->getClientOriginalName());
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }
    public static function pdfUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('theme/uploads/magazines/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = str_replace(" ","",$inputname->getClientOriginalName());
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }
}
