<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
class BlogNews extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog_news';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['blog_news_name', 'blog_news_img', 'blog_news_date', 'blog_news_url', 'blog_news_paper_name','blog_status','blog_news_des'];

    public static function removeBlogNews($id)
    {
        $file = BlogNews::findOrFail($id);

        if ($file->blog_news_img != '') {
            File::delete('theme/uploads/blog_news/' . $file->blog_news_img);
        }
        BlogNews::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('theme/uploads/blog_news/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


    $extension = str_replace(" ","",$inputname->getClientOriginalName());
    $fileName = time() . $extension;
    $inputname->move($uploadPath, $fileName);

        return $fileName;
    }
}
