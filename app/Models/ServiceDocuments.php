<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ServiceDocuments extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_documents';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'sd_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sd_service_id',
        'sd_title',
        'sd_doc_name',
        'sd_status',
    ];


    public function getService()
    {
        return $this->belongsTo(Services::Class, 'sd_service_id', 's_id');
    }

}
