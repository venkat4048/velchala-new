<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ProductSkus extends Model
{
    protected $fillable = [
        'sku_product_id',
        'sku_no',
        'sku_type',
        'sku_option',
        'sku_vendor_price',

        'sku_vdc_commission_type',
        'sku_vdc_commission_value',
        'sku_vdc_final_price',

        'sku_store_discount_type',
        'sku_store_discount_value',
        'sku_store_price',

    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_skus';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'sku_id';


    public static function updateInSkusFilters($skus)
    {
        $rerurnArray = array();

        if (count($skus) > 0) {
            foreach ($skus AS $sku) {
                if ($sku['for'] == 'edit') {
                    $rerurnArray['edit'][] = $sku;
                } elseif ($sku['for'] == 'new') {
                    $rerurnArray['new'][] = $sku;
                }
            }
        }

        return $rerurnArray;
    }


    public static function skusInsert($newSkus, $product_id)
    {
        $dbchanges = false;
        if (count($newSkus) > 0) {

            foreach ($newSkus as $options) {

                if (isset($options['sku_option']) && $options['sku_vendor_price'] != '') {
                    $optiondata = array();
                    $optiondata['sku_no'] = Str::random();
                    $optiondata['sku_product_id'] = $product_id;
                    $optiondata['sku_type'] = $options['sku_type'];
                    $optiondata['sku_option'] = $options['sku_option'];
                    $optiondata['sku_vendor_price'] = $options['sku_vendor_price'];
                    ProductSkus::create($optiondata);

                    $dbchanges = true;

                }
            }
        }

        if ($dbchanges== true && $product_id != '') {

            $productUpdate = array(
                'p_status' => 0
            );

            Products::where('p_id', $product_id)->update($productUpdate);
        }

    }

    public static function skusUpdate($editskus, $serverSkus, $product_id)
    {
        $dbchanges = false;

        $rerurnArray = array();

        if (count($editskus) > 0) {


            foreach ($editskus AS $editsku) {

                $serverSkuskey = array_search($editsku['sku_id'], array_column($serverSkus, 'sku_id'));

                if (
                    $serverSkus[$serverSkuskey]['sku_type'] != $editsku['sku_type'] ||
                    $serverSkus[$serverSkuskey]['sku_option'] != $editsku['sku_option'] ||
                    $serverSkus[$serverSkuskey]['sku_vendor_price'] != $editsku['sku_vendor_price']
                ) {


                    $dbchanges = true;

                    $forUpdateSku = $editsku;

                    unset($forUpdateSku['for']);
                    unset($forUpdateSku['sku_id']);

                    ProductSkus::where('sku_id', $editsku['sku_id'])->update($forUpdateSku);

                }


            }

            if ($dbchanges == true) {

                if ($product_id != '') {

                    $productUpdate = array(
                        'p_status' => 0
                    );

                    Products::where('p_id', $product_id)->update($productUpdate);
                }


//                dd($sku_product_id);
            }


        }
        return $rerurnArray;
    }


}
