<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'poems';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['poem_image', 'poem_name', 'gal_cat_id '];

   



}
