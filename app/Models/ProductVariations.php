<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_variations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pv_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pv_prod_id', 'pv_sku', 'pv_color','pv_weight','pv_mrp','pv_selling_price','pv_status'];
}
