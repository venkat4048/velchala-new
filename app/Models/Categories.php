<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'publishers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pub_name',
        'pub_slug',
        'pub_image',
        'pub_description',
        'pub_status',
        'id'

        ];

    public function getProducts()
    {
        return $this->hasMany(Products::Class,'publisher_id', 'id');
    }


   

}
