<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
   
    public function handle($request, Closure $next, $guard = null)
    {
       
        $routname = request()->segment(1);

        if ($routname == 'admin') {
            $adminuser = Auth::guard('admin')->user();
        }  else {
            $user = Auth::guard('web')->user();
        }

        if (!empty($adminuser)) {
            return redirect()->route('dashboard');
        }
        
        if (!empty($user)) {
            return redirect()->route('userprofile');
        }





        return $next($request);
    }


}
