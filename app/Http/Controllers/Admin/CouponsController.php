<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coupons;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CouponsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $action =Coupons::destroy($requestData['coupon_id']);
            if ($action == true) {
                $msg = "Coupon deleted successfully!";
            } else {
                $msg = "Coupon delete Error!";

            }
            return redirect()->route('coupons')->with('flash_message', $msg);
        } else {

            $coupons = Coupons::with('getCoupon')->orderBy('coupon_id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'coupons';
        $data['sub_active_menu'] = 'coupons-list';
        $data['title'] = 'Coupons';
        $data['coupons'] = $coupons;
        return view('backend.coupons.list', $data);
    }

    public function addNewCoupons(Request $request, $id = null)
    {

        if ($request->isMethod('post')) {

            $requestData = $request->all();

//            dd($requestData);

            $request->validate([
                'coupon_code' => 'required|unique:coupons',
                'coupon_discount_value' => 'required',
                'coupon_discount_type' => 'required',
                'coupon_quantity' => 'required',
                'coupon_country' => 'required',
                'coupon_usage_type' => 'required',
                'coupon_expiry_date' => 'required',
                'coupon_status' => 'required'
            ], [
                'coupon_code.required' => 'Please enter code',
                'coupon_code.unique' => 'This coupon code already exist',
                'coupon_discount_value.required' => 'Please enter coupon discount value',
                'coupon_discount_type.required' => 'Please select discount type',
                'coupon_quantity.required' => 'Please enter coupon quantity',
                'coupon_country.required' => 'Please select coupon country',
                'coupon_usage_type.required' => 'Please select usage type',
                'coupon_expiry_date.required' => 'Please select expiry date',
                'coupon_status.required' => 'Select status',
            ]);

//            Validator::make($request->all(), [
//                'coupon_code' => 'required|unique:coupons',
//                'coupon_discount_value' => 'required',
//                'coupon_discount_type' => 'required',
//                'coupon_quantity' => 'required',
//                'coupon_country' => 'required',
//                'coupon_usage_type' => 'required',
//                'coupon_expiry_date' => 'required',
//                'coupon_status' => 'required'
//            ], [
//                'coupon_code.required' => 'Please enter code',
//                'coupon_code.unique' => 'This coupon code already exist',
//                'coupon_discount_value.required' => 'Please enter coupon discount value',
//                'coupon_discount_type.required' => 'Please select discount type',
//                'coupon_quantity.required' => 'Please enter coupon quantity',
//                'coupon_country.required' => 'Please select coupon country',
//                'coupon_usage_type.required' => 'Please select usage type',
//                'coupon_expiry_date.required' => 'Please select expiry date',
//                'coupon_status.required' => 'Select status',
//            ]);


            $requestData['coupon_expiry_date']=date("Y-m-d", strtotime($requestData['coupon_expiry_date']));

            if ($requestData['coupon_id'] == '') {

                Coupons::create($requestData);

                $mes = 'Coupon added successfully!';
            } else {
                $coupons = Coupons::findOrFail($requestData['coupon_id']);
                $coupons->update($requestData);
                $mes = 'Coupon updated successfully!';
            }
            return redirect()->route('coupons')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['coupon_id'] = '';
            $data['coupon'] = '';
            if ($id) {
                $data['coupon_id'] = $id;
                $data['coupon'] = Coupons::findOrFail($id);
            }
            $data['active_menu'] = 'coupons';
            $data['sub_active_menu'] = 'manage-coupons';
            $data['title'] = 'Manage coupons';
            return view('backend.coupons.add', $data);
        }
    }

    public function checkCouponcode(Request $request)
    {

        switch ($request['type']) {
            case 'edit':
                $current_coupon_code = $request['current_coupon_code'];
                if ($request['coupon_code'] === $current_coupon_code) {
                    echo 'true';
                } else {
                    $countWeb = Coupons::where('coupon_code', $request['coupon_code'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Coupons::where('coupon_code', $request['coupon_code'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }
}
