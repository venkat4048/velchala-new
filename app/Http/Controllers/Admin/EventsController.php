<?php

namespace App\Http\Controllers\Admin;

use App\Models\JayanthiEvents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;

class EventsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = JayanthiEvents::removeEvent($requestData['id']);

            if ($action == true) {
                $msg = "Event deleted successfully!";
            } else {
                $msg = "Event delete Error!";

            }
            return redirect()->route('events')->with('flash_message', $msg);
        } else {

            $events =JayanthiEvents::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'events';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'events';
        $data['events'] = $events;
        return view('backend.events.list', $data);
    }

    public function addNewEvents(Request $request, $id = null)
    {
        $tableInfo = new JayanthiEvents();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['id'] != '') {
                $events = JayanthiEvents::findOrFail($requestData['id']);
                $imageRule = empty($events->img) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [

                'event_name' => 'required',
                'event_des' => 'required',
                'img' => $imageRule,
                'event_date' => 'required',
                'event_location' => 'required',
            ], [
                'event_name.required' => 'Please enter title',
                'event_des.required' => 'Please enter description',
                'img.required' => 'select image to upload',
                'event_date.required' => 'Select Date',
                'event_location.required' => 'Select Date',
            ]);
               $requestData['event_url'] = str_slug($requestData['event_name']);
            if ($requestData['id'] == '') {
                if ($request->hasFile('img')) {
                    $fileName = JayanthiEvents::imageUpload($request['img']);
                    $requestData['img'] = $fileName;
                }

                JayanthiEvents::create($requestData);

                $mes = 'Event added successfully!';
            } else {

                if ($request->hasFile('img')) {

                    $fileName = JayanthiEvents::imageUpload($request['img'], $events->img);
                    $requestData['img'] = $fileName;
                }

                $events->update($requestData);
                $mes = 'Events updated successfully!';
            }
            return redirect()->route('events')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['id'] = '';
            $data['event'] = '';
            if ($id) {
                $data['id'] = $id;
                $data['event'] = JayanthiEvents::findOrFail($id);
            }
            $data['active_menu'] = 'events';
            
            $data['title'] = 'Manage Events';
            return view('backend.events.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = JayanthiEvents::findOrFail($request['image']);

        File::delete('theme/uploads/events/' . $image->img);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
