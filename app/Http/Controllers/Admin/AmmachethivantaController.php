<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ammachethivanta;
use App\Models\AmmachethivantaOrderItems;
use App\Models\AmmachethivantaOrders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Validator;


class AmmachethivantaController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

            $action = Ammachethivanta::removeRecord($requestData['ac_id']);

            if ($action == true) {
                $msg = "Record deleted successfully!";
            } else {
                $msg = "Record delete Error!";

            }
            return redirect()->route('ammachethivanta')->with('flash_message', $msg);
        } else {

            $records = Ammachethivanta::orderBy('ac_id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'ammachethivanta';
        $data['sub_active_menu'] = 'ammachethivanta-list';
        $data['title'] = 'Order Amma Cheti Vanta at Vdesiconnect';
        $data['records'] = $records;
        return view('backend.ammachethivanta.list', $data);
    }

    public function addNewRecords(Request $request, $id = null)
    {
        $tableInfo = new Ammachethivanta();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['ac_id'] != '') {
                $records = Ammachethivanta::findOrFail($requestData['ac_id']);
                $imageRule = empty($records->ac_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [
                'ac_name' => 'required',
                'ac_description' => 'required',
                'ac_image' => $imageRule,
                'ac_status' => 'required'
            ], [
                'ac_name.required' => 'Please enter title',
                'ac_description.required' => 'Please enter description',
                'ac_image.required' => 'select image to upload',
                'ac_status.required' => 'Select status',
            ]);


            if ($requestData['ac_id'] == '') {
                if ($request->hasFile('ac_image')) {
                    $fileName = Ammachethivanta::imageUpload($request['ac_image']);
                    $requestData['ac_image'] = $fileName;
                }

                $id = Ammachethivanta::create($requestData)->ac_id;


                $mes = 'Record added successfully!';
            } else {

                if ($request->hasFile('ac_image')) {
                    $fileName = Ammachethivanta::imageUpload($request['ac_image'], $records->ac_image);
                    $requestData['ac_image'] = $fileName;
                }

                $records->update($requestData);


                $mes = 'Record updated successfully!';
            }
            return redirect()->route('ammachethivanta')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['ac_id'] = '';
            $data['record'] = '';
            if ($id) {
                $data['ac_id'] = $id;
                $data['record'] = Ammachethivanta::findOrFail($id);
            }
            $data['active_menu'] = 'ammachethivanta';
            $data['sub_active_menu'] = 'ammachethivanta-list';
            $data['title'] = 'Manage Amma Chethi vanta module';
            return view('backend.ammachethivanta.add', $data);
        }
    }

    public function deleteimage(Request $request)
    {
        $image = Ammachethivanta::findOrFail($request['image']);

        File::delete('uploads/ammachethivanta/' . $image->ac_image);

        $update_data = array();
        $update_data['ac_image'] = '';
        $image->update($update_data);
        exit();
    }

    public function orders($id = null,Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            AmmachethivantaOrders::destroy($requestData['ac_order_id']);
            AmmachethivantaOrderItems::where('acoitem_order_id', $requestData['ac_order_id'])->delete();
            return redirect()->route('ammachethivantaorders')->with('flash_message', 'Order deleted successfully!');

        }
        $data = array();
        $data['active_menu'] = 'ammachethivanta';
        $data['sub_active_menu'] = 'ammachethivanta-orders';
        $data['title'] = 'Manage Amma Chethi vanta orders';
        if ($id) {
            $data['orders'] = AmmachethivantaOrders::with('orderItems')->where('ac_order_id', $id)->first();
            return view('backend.ammachethivanta.orderDetails', $data);
        } else {
            $data['records'] = AmmachethivantaOrders::with('orderItems')->orderBy('ac_order_id', 'DESC')
                ->paginate(PAGE_LIMIT);
            return view('backend.ammachethivanta.orders', $data);
        }

    }

    public function changeOrderStatus(Request $request, $id)
    {
        $retuls = AmmachethivantaOrders::changeOrderStatus($request);

        if ($retuls == 1) {
            $mes = "Status Updated";

        } else {

            $mes = "Status change failed";
        }

        return redirect()->back()->with('flash_message', $mes);


//        dump($ammachethiOrder);

    }


}
