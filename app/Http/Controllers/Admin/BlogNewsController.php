<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogNews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;

class BlogNewsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = BlogNews::removeBlogNews($requestData['id']);

            if ($action == true) {
                $msg = "Megazine deleted successfully!";
            } else {
                $msg = "Megazine delete Error!";

            }
            return redirect()->route('admin_blog_news')->with('flash_message', $msg);
        } else {

            $adminmagazines =BlogNews::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'blog_news';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'Adminmagazines';
        $data['blogs_news'] = $adminmagazines;
        return view('backend.blog_news.list', $data);
    }

    public function addNewBlogNews(Request $request, $id = null)
    {
        $tableInfo = new BlogNews();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['id'] != '') {
                $blognews = BlogNews::findOrFail($requestData['id']);
                $imageRule = empty($blognews->blog_news_img) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [

                'blog_news_name' => 'required',
                'blog_news_date' => 'required',
                'blog_news_img' => $imageRule,
                'blog_news_paper_name' =>'required',
                'blog_news_url' => 'required',
                
            ], [
                'blog_news_name.required' => 'Please enter title',
                'blog_news_date.required' => 'Please enter Date',
                'blog_news_paper_name.required' => 'Please enter Paper Name',
                'blog_news_url.required' => 'Please enter Paper Url',
               
            ]);
              
            if ($requestData['id'] == '') {
                if ($request->hasFile('blog_news_img')) {
                    $fileName = BlogNews::imageUpload($request['blog_news_img']);
                    $requestData['blog_news_img'] = $fileName;
                }
               

                BlogNews::create($requestData);

                $mes = 'Event added successfully!';
            } else {

                if ($request->hasFile('blog_news_img')) {

                    $fileName = BlogNews::imageUpload($request['blog_news_img'], $blognews->blog_news_img);
                    $requestData['blog_news_img'] = $fileName;
                }
                

                $blognews->update($requestData);
                $mes = 'Magazine updated successfully!';
            }
            
            return redirect()->route('admin_blog_news')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['id'] = '';
            $data['blognews'] = '';
            if ($id) {
                $data['id'] = $id;
                $data['blognews'] = BlogNews::findOrFail($id);
            }
            $data['active_menu'] = 'blog_news';
            
            $data['title'] = 'Manage Events';
            return view('backend.blog_news.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = BlogNews::findOrFail($request['blog_news_img']);

        File::delete('theme/uploads/blog_news/' . $image->blog_news_img);

        $update_data = array();
        $update_data['blog_news_img'] = '';
        $image->update($update_data);
        exit();
    }
}
