<?php

namespace App\Http\Controllers\Admin;

use App\Models\Magazines;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;

class MagazinesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Magazines::removeMagazine($requestData['id']);

            if ($action == true) {
                $msg = "Megazine deleted successfully!";
            } else {
                $msg = "Megazine delete Error!";

            }
            return redirect()->route('adminmagazines')->with('flash_message', $msg);
        } else {

            $adminmagazines =Magazines::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'adminmagazines';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'Adminmagazines';
        $data['adminmagazines'] = $adminmagazines;
        return view('backend.magazines.list', $data);
    }

    public function addNewMagazines(Request $request, $id = null)
    {
        $tableInfo = new Magazines();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['id'] != '') {
                $magazines = Magazines::findOrFail($requestData['id']);
                $imageRule = empty($events->mag_profile_pic) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [

                'mag_profile_pic' => 'required',
                'mag_pdf' => 'required',
                'mag_date' => $imageRule,
                
            ], [
                'mag_profile_pic.required' => 'Please enter title',
                'mag_pdf.required' => 'Please enter description',
                'mag_date.required' => 'select image to upload',
               
            ]);
              
            if ($requestData['id'] == '') {
                if ($request->hasFile('mag_profile_pic')) {
                    $fileName = Magazines::imageUpload($request['mag_profile_pic']);
                    $requestData['mag_profile_pic'] = $fileName;
                }
                if ($request->hasFile('mag_pdf')) {
                    $fileName = Magazines::pdfUpload($request['mag_pdf']);
                    $requestData['mag_pdf'] = $fileName;
                }

                Magazines::create($requestData);

                $mes = 'Event added successfully!';
            } else {

                if ($request->hasFile('mag_profile_pic')) {

                    $fileName = Magazines::imageUpload($request['mag_profile_pic'], $magazines->mag_profile_pic);
                    $requestData['mag_profile_pic'] = $fileName;
                }
                if ($request->hasFile('mag_pdf')) {

                    $fileName = Magazines::pdfUpload($request['mag_pdf'], $magazines->mag_pdf);
                    $requestData['mag_pdf'] = $fileName;
                }

                $magazines->update($requestData);
                $mes = 'Magazine updated successfully!';
            }
            
            return redirect()->route('adminmagazines')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['id'] = '';
            $data['magazines'] = '';
            if ($id) {
                $data['id'] = $id;
                $data['magazines'] = Magazines::findOrFail($id);
            }
            $data['active_menu'] = 'adminmagazines';
            
            $data['title'] = 'Manage Events';
            return view('backend.magazines.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = Magazines::findOrFail($request['image']);

        File::delete('theme/uploads/events/' . $image->img);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
