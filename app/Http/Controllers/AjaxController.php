<?php

namespace App\Http\Controllers;

use App\Models\AmmachethivantaSKUS;
use App\Models\Categories;
use App\Models\City;
use App\Models\Countries;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductSkus;
use App\Models\States;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }


    public function ajax_ammchathivanta_wightbased_price(Request $request)
    {
        $wight = $request->get('wight');

        $returnArray = array();

        $returnArray['wights'] = array_keys(ammaChethiVantaWightsData());
        $returnArray['currency'] = currencySymbol(getCurency());
        $returnArray['current_wight'] = $wight;
        $returnArray['price'] = curencyConvert(getCurency(), ammaChethiVantaWightBasePrice($wight));

        if (in_array($wight, $returnArray['wights'])) {
            $returnArray['checkout_btn'] = 'on';

        } else {
            $returnArray['checkout_btn'] = 'off';

        }


        return json_encode($returnArray);

    }


    public function ajaxCurencyConvert(Request $request)
    {
        $requestData = $request->all();
//        dd($requestData);

        echo curencyConvert('USD', $requestData['price']);
        exit();
    }

    public function removeProductOptions(Request $request)
    {
        $requestData = $request->all();
        ProductSkus::destroy($requestData['id']);
        exit();
    }


    public function getammachethivantaoptions(Request $request)
    {
        $requestData = $request->all();
//        dd($requestData);

        $details = AmmachethivantaSKUS::where('as_id', $requestData['id'])->first();
        echo curencyConvert('USD', $details->as_price);
        exit();
    }


    public function search(Request $request)
    {

        $name = $request->term;

//        dd($name);

        $products = Products::orderBy('p_id', 'desc')
            ->when($name, function ($query) use ($name) {
                return $query->whereRaw("LOWER(p_name) LIKE ?", '%' . strtolower($name) . '%');
            })
            ->where('p_status', 1)
            ->limit(10)
            ->get();
//            ->paginate(10);


        $returnArray = array();

        if (count($products) > 0) {
            foreach ($products AS $product) {


                if (count($product->productImages) > 0) {
                    $imageID = array_first($product->productImages)->pi_id;
                    $image_url = getImagesById($imageID);
                    if ($image_url == '') {
                        $image_url = '/frontend/images/noproduct-available.jpg';
                    }
                } else {
                    $image_url = '/frontend/images/noproduct-available.jpg';
                }


                if (count($product->productSKUs) > 0) {

                    $priceSKU = $product->productSKUs[0];
                }

//                dd($product->getCategory->category_alias);

                $product_build = array();
                $product_build['name'] = $product->p_name;
                $product_build['image'] = $image_url;
//                $product_build['image']=   getProductImage($product->product_id)->pi_image_name;
                $product_build['alias'] = $product->p_alias;
                $product_build['real_price'] = currencySymbol('USD') . '' . curencyConvert('USD', $priceSKU->sku_vdc_final_price);;
                $product_build['sele_price'] = currencySymbol('USD') . ' ' . curencyConvert('USD', $priceSKU->sku_store_price);
                $product_build['product_url'] = route('productPage', ['category' => $product->getCategory->category_alias, 'product' => $product->p_alias]);

                $returnArray[] = $product_build;
            }
        } else {
            $returnArray = null;
        }


        print json_encode($returnArray);

//        echo "<pre>";
//        print_r(dd($products));
        exit();
    }


    public function showCategoryTypes(Request $request)
    {
        $types = Categories::where('category_status', 'active')->where('category_parent_id', $request->catID)->where('category_options', 'types')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
//        return json_encode($types);
        $others = Categories::where('category_status', 'active')->where('category_parent_id', $request->catID)->where('category_options', 'others')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
//        return json_encode($others);

        return array(
            'types' => $types,
            'others' => $others,
        );


    }


    public function showStates(Request $request)
    {
        $states = States::where('state_status', 'active')->where('state_country_id', $request->catID)->orderBy('state_id', 'asc')->pluck('state_name', 'state_id');

        return array(
            'states' => $states,
        );


    }

    public function showCities(Request $request)
    {
        $cities = City::where('city_status', 'active')->where('city_state_id', $request->catID)->orderBy('city_id', 'asc')->pluck('city_name', 'city_id');

        return array(
            'cities' => $cities,
        );


    }


    public function productImageRemove(Request $request)
    {
//        dd($request['imageid']);

        $removeimage = ProductImages::removeImage($request['imageid']);
        echo $removeimage;
    }


    public function checkProductAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_product_alias = $request['current_product_alias'];
                if ($request['p_alias'] === $current_product_alias) {
                    echo 'true';
                } else {
                    $countWeb = Products::where('p_alias', $request['p_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Webseries::where('w_alias', $request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }


}
