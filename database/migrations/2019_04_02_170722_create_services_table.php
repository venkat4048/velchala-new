<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('s_id');
            $table->string('s_contact_person');
            $table->string('s_address_line_1');
            $table->string('s_address_line_2');
            $table->string('s_country');
            $table->string('s_state');
            $table->string('s_city');
            $table->string('s_options');
            $table->string('s_date');
            $table->string('s_time');
            $table->string('s_email');
            $table->string('s_phone');
            $table->text('s_msg');
            $table->string('s_user_id')->nullable();
            $table->string('s_type')->comment('medical,tutor,property,visa,insurance');
            $table->string('s_duration')->nullable();
            $table->date('s_duedate')->nullable();
            $table->string('s_no_of_days')->nullable();
            $table->string('s_unit_price')->nullable();
            $table->string('s_unit_type')->nullable();
            $table->string('s_sub_total')->nullable();
            $table->string('s_tax')->nullable();
            $table->string('s_total')->nullable();
            $table->string('s_status')->nullable()->comment('null,pending,paid');
            $table->string('s_payment_invoice_num')->nullable();
            $table->string('s_payment_transaction_id')->nullable();
            $table->longText('s_payment_payer')->nullable();
            $table->longText('s_payment_transactions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
