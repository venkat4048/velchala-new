<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccassionReminderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occassion_reminder', function (Blueprint $table) {
            $table->bigIncrements('or_id');
            $table->text('or_user_id');
            $table->text('or_name');
            $table->text('or_type');
            $table->date('or_date');
            $table->text('or_remind');
            $table->text('or_desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occassion_reminder');
    }
}
