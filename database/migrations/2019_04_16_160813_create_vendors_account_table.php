<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendorsAccount', function (Blueprint $table) {
            $table->increments('va_id');
            $table->string('va_vendor_id')->nullable();
            $table->string('va_display_name')->nullable();
            $table->text('va_business_desc')->nullable();
            $table->text('va_address_1')->nullable();
            $table->text('va_address_2')->nullable();
            $table->text('va_state')->nullable();
            $table->text('va_city')->nullable();
            $table->text('va_pincode')->nullable();
            $table->text('va_name')->nullable();
            $table->text('va_mobile')->nullable();
            $table->text('va_email')->nullable();
            $table->text('va_time_from')->nullable();
            $table->text('va_time_to')->nullable();
            $table->text('va_status')->comment('pending,approved,reject')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendorsAccount');
    }
}
