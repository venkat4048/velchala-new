<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmmachethivantaOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ammachethivanta_order_items', function (Blueprint $table) {
            $table->bigIncrements('acoitem_id');
            $table->integer('acoitem_order_id');
            $table->integer('acoitem_product_id');
            $table->float('acoitem_weight');
            $table->float('acoitem_product_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ammachethivanta_order_items');
    }
}
