<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->increments('pv_id');
            $table->integer('pv_prod_id');
            $table->string('pv_sku')->nullable();
            $table->string('pv_color')->nullable();
            $table->string('pv_weight')->nullable();
            $table->string('pv_mrp');
            $table->string('pv_selling_price');
            $table->string('pv_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}
