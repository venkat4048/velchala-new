<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorBusinessDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendorBusinessDetails', function (Blueprint $table) {
            $table->increments('vb_id');
            $table->string('vb_vendor_id')->nullable();
            $table->string('vb_name')->nullable();
            $table->string('vb_tan')->nullable();
            $table->string('vb_gst')->nullable();
            $table->text('vb_address1')->nullable();
            $table->text('vb_address2')->nullable();
            $table->string('vb_state')->nullable();
            $table->string('vb_city')->nullable();
            $table->string('vb_pincode')->nullable();
            $table->string('vb_account_name')->nullable();
            $table->string('vb_account_number')->nullable();
            $table->string('vb_ifsc')->nullable();
            $table->string('vb_bank')->nullable();
            $table->string('vb_branch')->nullable();
            $table->string('vb_bank_state')->nullable();
            $table->string('vb_bank_city')->nullable();
            $table->string('vb_type')->nullable();
            $table->string('vb_pan')->nullable();
            $table->string('vb_address_proof')->nullable();
            $table->string('vb_cheque')->nullable();
            $table->string('vb_status')->comment('pending,approved,reject')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendorBusinessDetails');
    }
}
