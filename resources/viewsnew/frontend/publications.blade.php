<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
   <div id="alertAddWishlist-Detail" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
  <strong><span class="icon-check"></span> Success!</strong>  Added Successfully to your Wishlist.  <a class="d-block" href="{{url('/user_wishlist')}}"><strong>View Now</strong></a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span></button></div>
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container wow animate__animated animate__fadeInDown">
                <h1>Publications</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Publications</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       @if(count($publications)>0)
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-md-4 align-self-center">
                                <!--<p class="pb-0">1 – 40 of 75 results</p>-->
                                <p class="pb-0">{{ count($publications)}} results</p>
                            </div>
                            <!--/col -->
                             <!-- col -->
                             <div class="col-md-8 publications-filters">
                                <div class="form-group">
                                     <select data-id="lang" class="form-control filter">  
                                        <option>Language</option>                                      
                                         <option>Telugu </option>
                                         <option>English </option>                                                                     
                                     </select>
                                 </div>
                                <div class="form-group">
                                     <select data-id="publi" class="form-control filter">  
                                        <option>Publications</option>                                      
                                         <option value="Jayanthi">Jayanthi </option>
                                         <option value="Sister Niveditha">Sister Niveditha </option>                                                                     
                                     </select>
                                 </div>
                                 <div class="form-group">
                                     <select data-id="multiple" class="form-control filter" >
                                         <option value="Sort by">Sort by</option>
                                         <option value="Popular Books">Popular Books</option>   
                                         <option value="Newest First">Newest First</option>   
                                         <option value="Alphabet A-Z">Alphabet A-Z</option>                                     
                                     </select>
                                 </div>
                             </div>
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div  class="publications-items">
                    <!-- container -->
                    <div class="container">                   
                    <!-- alert wishlist-->
                    <div id="alertAddWishlist" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Wishlist.  <a href="javascript:void(0)" class="d-block"><strong>View Now</strong></a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--/ alert wishlist -->
                        <!-- row -->
            <div id="result_publications_ajax" class="row py-3">
                <!-- col -->

               @foreach($publications as $value)
               
                <div class="col-6 col-sm-4 col-md-3">
                    <div class="book-item">
                        <figure class="bookcover">
                            <a href="{{ url('details',['catalias'=>$value->publication_slug]) }}">
                                <img src="theme/uploads/publications/{{$value->img}}" alt="" class="img-fluid">
                            </a>
                            <div id="wishlist_{{$value->publi_id}}" data-publ-id="{{$value->publi_id}}" class="wishlist-icon wishlist-add" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                <span   class="icon-heart-o icomoon "></span>
                            </div>
                            <span class="badge badge-pill badge-success">{{$value->pub_name}}</span>
                        </figure>
                        <article class="text-center">
                            <a href="{{ url('details',['publications_name_url'=>$value->publication_slug]) }}">{{$value->publication_name}}</a>
                            <p class="price text-center">
                                <span class="offer">{{ $value->price-$value->dis_price}}</span>
                                <span class="oldprice">{{$value->price}}</span>
                            </p>
                        </article>
                    </div>
                </div>
                @endforeach
                    
                    <!--/ col -->                           
                </div>
                        

                <!-- row -->
               
                <!--/ row -->
            </div>
            <!--/ container -->
            </div>
            <!--/ publications list items -->
        </div>
        <!--/ publications -->

   </div>
   @else
   <div class="col-md-6 text-center no-data ">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            </div>
    @endif
   <!--/ sub page body -->



</main> 
    <!--/ main-->
     <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         $(".filter").change(function() {

            filter = $(this).val();
             filterType = $(this).attr("data-id");

             data = 'filter='+filter+'&filterType='+filterType+"&_token=<?php echo csrf_token() ?>";

            $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                  
                  $("#result_publications_ajax").html(results);
               }
            });
         });
      </script>
<script>
         $(".wishlist-add").click(function() {

             publicationId = $(this).attr("data-publ-id");
             publi_id ="#wishlist_"+publicationId;
             data = 'publicationId='+publicationId+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/ajax_wish_list',
               data:data,
               success:function(results) {

                   $(publi_id).css('background-color','#007aff');
                   $(publi_id).removeClass('wishlist-add');
                   
                   $("#alertAddWishlist-Detail").show();
               }
            });
         });
   </script>
   @stop

   <script>
    $(document).ready(function(){        
        $('.wishlist-icon').click(function(){
            $('#alertAddWishlist').show();
        }) 
    });
   </script>
    </body>
</html>