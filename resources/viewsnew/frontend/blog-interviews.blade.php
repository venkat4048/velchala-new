<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.blog_layout')

  @section('content')
    <!--main-->   
    <main class="blog-main">

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
            @if(count($video))
            @foreach($video as $value)
            <div class=" col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                <div class="card ">
                    <iframe width="100%" height="225" src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">                       
                        <p class="card-text pb-3">Kondal Rao Interview on his Personal Life</p>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif 
            <!--/ col -->

          
            <!--/ col -->

        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    @stop
    </body>
</html>