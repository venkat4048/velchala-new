<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Jayanthi Magazine</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Magazines</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-sm-6 align-self-center">
                                <p class="pb-0">{{ count($magazines) }} results</p>
                            </div>
                            <!--/col -->
                             <!-- col -->
                             <!--
                             <div class="col-sm-6 justify-content-end">
                                 <div class="form-group mb-0 float-right">
                                     <select class="form-control">
                                         <option>Choose Year</option>
                                         <option>2015</option>
                                         <option>2016</option>
                                         <option>2017</option>
                                         <option>2018</option>   
                                         <option>2019</option>                                     
                                     </select>
                                 </div>
                             </div>-->
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div class="publications-items">
                    <!-- container -->
                    <div class="container">
                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                         @if(count($magazines)>0)
                         @foreach($magazines as $val)
                          <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInUp">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a href="theme/magazines/{{ $val->mag_pdf }}" target="_blank">
                                            <img src="theme/magazines/{{ $val->mag_profile_pic }}" alt="" class="img-fluid">
                                        </a>  
                                    </figure>
                                    <article class="text-center">
                                        <a href="theme/magazines/{{ $val->mag_pdf }}" target="_blank">{{ date("M Y",strtotime($val->mag_date)) }}</a>
                                    </article>
                                </div>
                            </div>
                           @endforeach
                           @else
                           <div class="row justify-content-center no-data">
                            <div class="col-md-6 text-center">
                                <h2 class="h2">No Data Available Now</h2>
                                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                <p>Thank you for visit us</p>
                            </div>
                            </div>
                           @endif
                            <!--/ col -->                                                   
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container -->
                </div>
                <!--/ publications list items -->
            </div>
            <!--/ publications -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    @stop
    </body>
</html>