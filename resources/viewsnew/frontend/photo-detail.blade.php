<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Album Name will be here</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="photo-albums.php">Photo Album</a></li>                   
                        <li class="breadcrumb-item active" aria-current="page"><span>Album Name will be here</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                    
                      @foreach($photos as $value) 
                    <div class="col-sm-6 col-md-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="book-item albumitem">
                            <figure class="bookcover">
                                <a href="">
                                    <img src="{{ url('theme/uploads/photos').'/'.$value->photo_img }}" alt="" class="img-fluid">
                                </a>                              
                                <span class="badge badge-pill photosnumber"></span>
                            </figure>
                            <article>
                                <h2 class="h5">
                                    <a href="">{{ $value->event_name }}</a>
                                </h2>
                                <div class="item-deails d-flex flex-wrap">
                                    <p class="small"><span class="icon-pin icomoon"></span>{{ $value->event_location }}</p>
                                    <p class="small pl-4"><span class="icon-calendar icomoon pr-1"></span>{{ date("d-m-Y",strtotime($value->event_date)) }}</p>
                                </div> 
                            </article>
                        </div>
                    </div>
                      @endforeach     
                    <!--/ col --> 

                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    @stop
    </body>
</html>