<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
       <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- <h1>Publications</h1> -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('/publications')}}">Publications</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>{{  ucfirst($publications->publication_name)}}</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

            <!-- top publication detail -->
            <div class="book-top-detail">
                <!-- container -->
                <div class="container">
                   
                    <!-- card-->
                    <div class="card p-2 p-sm-5">
                        <!-- alert wishlist-->
                        <div id="alertAddWishlist-Detail" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Wishlist.  <a class="d-block" href="{{url('/user_wishlist')}}"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert wishlist -->

                         <!-- alert add to cart-->
                         <div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert add to cart -->


                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6">
                                <figure class="figure-detail">
                                    <img src="{{ url('theme/uploads/publications').'/'.$publications->img }}" alt="" class="img-fluid">
                                </figure>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-6 book-detail-rt align-self-center">
                                 <!-- border -->
                                <div class="px-4 py-3 border">
                                    <!-- row -->
                                    <div class="row position-relative">
                                        <div class="col-md-9 col-12">
                                            <h1 class="h2 ptregular">{{  ucfirst($publications->publication_name)}}</h1>
                                           <!-- <p class="pb-1">
                                                <span class="badge badge-success p-1">4.4 <span class="icon-star icomoon"></span> </span>
                                                <span class="fgray">(25 Reviews)</span>
                                            </p>-->
                                            <p class="price pl-1">
                                                <span class="offer">{{  $publications->price-$publications->dis_price}}</span>
                                                <span class="oldprice">{{  $publications->price}}</span>
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-12 text-right ">

                                          @if(Auth::user())
                                            @if(isset($wishlist->publi_id))
                                            
                                             <div class="wishlist-icon" data-toggle="tooltip" data-placement="top" id="wishlist-icon" 
                                            style="background-color:#007aff" data-publ-id="{{ $publications->public_id }}" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div>
                                            @else
                                           <div class="wishlist-icon add-wishlist" data-toggle="tooltip" data-placement="top" id="wishlist-icon" data-publ-id="{{ $publications->public_id }}" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div>
                                            
                                            @endif

                                          @else
                                           <a href="{{url('login')}}"><div class="wishlist-icon"  data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div> </a>
                                          @endif  
                                        </div>
                                    </div>
                                    <!--/ row -->
                                    <p class="basic-description">{{  substr($publications->description,0,150)}} </p>
                                </div>
                                <!-- border/-->

                                <!-- border -->
                                <div class="px-4 border mt-2 wow animate__animated animate__fadeInDown">
                                    <!-- specifications -->
                                    <div class="book-specs">
                                     <!-- row -->
                                     <div class="row">
                                         <!--col-->
                                         <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Availability</dt>
                                                    <dd class="fgreen fsbold">In Stock</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>SKU Code</dt>
                                                    <dd>{{  $publications->sku_code }}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Pages</dt>
                                                    <dd>{{  $publications->pages }}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Author</dt>
                                                    <dd>{{ ucfirst($publications->author )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Language</dt>
                                                    <dd>{{ ucfirst($publications->language )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Publisher</dt>
                                                    <dd>{{ ucfirst($publications->pub_name )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Published Date</dt>
                                                    <dd>{{ date("d-m-Y",strtotime($publications->published_date ))}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col
                                          <div class="col-sm-4 col-6 brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>No. of Readers</dt>
                                                    <dd>{{ ucfirst($publications->pages )}}</dd>
                                                </dl>
                                            </div>
                                         </div>-->
                                         <!--/ col -->
                                     </div>
                                     <!--/ row -->
                                 </div>
                                 <!--/ specifications -->
                                </div>
                                <!--/ border -->

                                <p class="py-3">
                                    <span>QTY</span>
                                    <span>
                                        <input id="cart_quanity" value="1" class="text-center qty" type="number" placeholder="1">
                                    </span>
                                </p>

                                <a href="{{ url('theme/uploads/publications_pdf').'/'.$publications->pdf_file }}" class="orange-btn text-uppercase" target="_blank">Read Online</a>
                                <!-- <a id="addtokcart-icon" data-publ-id="{{ $publications->public_id }}"  class="orange-btn-border text-uppercase">Add to Cart</a> -->

                                <!-- social share -->
                                <div class="social-share pt-5">
                                    <span class="pt-1 span-share">Share this product</span>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                                </div>
                                <!--/ social share -->
                                
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/card ends -->

                    <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1">
                                <li>Description</li>
                                <li>Reviews</li>
                                <li>Videos</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <!-- description -->
                                <div>
                                    <p>{{ ucfirst($publications->description )}}
                                </div>
                                <!--/ description -->

                                <!-- reviews -->
                                <div>
                                    <h2 class="h4 ptregular pb-3 border-bottom">Customer Reviews <span>(0)</span></h2>
                                    <!-- user review -->
                                    <div class="user-reviewrow border-bottom">
                                       

                                    </div>
                                    <!--/ user review -->
                                     <!-- leave review -->
                                     <div class="leave-review">
                                        <form class="form">                                           
                                            <div class="form-group">                                               
                                                <textarea class="form-control" placeholder="Leave Your Review"></textarea>
                                            </div>
                                            <div class="rating"></div>
                                            <p><small>You have given rating 4/5</small></p>
                                        </form>
                                        <a href="javascript:void(0)" class="orange-btn-border text-uppercase">Submit REview</a>

                                    </div>
                                    <!--/ leave review -->
                                </div>
                                <!--/ reviews -->

                                <!-- related videos -->
                                <div>
                                    
                                    <div class="row justify-content-center">
                                      <div class="col-md-6 text-center">
                                          <h2 class="h2">No Data Available Now</h2>
                                          <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                          <p>Thank you for visit us</p>
                                          </div>
                                      </div>
                                    
                                     <!-- row -->
                                    
                                </div>
                                <!--/ related videos -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->

                    <!-- you may also interested in -->
                    <div class="similar-books">
                        <h2 class="h3 ptregular border-bottom py-3">You may also be interested in</h2>
                        <!-- row -->
                        <div class="row pt-3">                           
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ you may also interested in -->
                </div>
                <!--/ container-->
            </div>
       </div>
    </main> 
    <!--/ main-->

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         $("#addtokcart-icon").click(function() {

             publicationId = $(this).attr("data-publ-id");
             cart_quanity = $("#cart_quanity").val();
             data = 'publicationId='+publicationId+'&cart_quanity='+cart_quanity+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/add_to_cart_data_ajax',
               data:data,
               success:function(results) {
                  
                  
                   $('#alertAddtocart').show();
                   location.reload();
               }
            });
         });

      </script>

      <script>
         $(".add-wishlist").click(function() {

             publicationId = $(this).attr("data-publ-id");
             
             data = 'publicationId='+publicationId+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/ajax_wish_list',
               data:data,
               success:function(results) {
                  
                  $("#wishlist-icon").css('background-color','#007aff');
                  $("#wishlist-icon").removeClass('add-wishlist');
                  $('#alertAddWishlist-Detail').show();
               }
            });
         });

      </script>
    @stop
    </body>
</html>