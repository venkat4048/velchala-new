<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
     @extends('includes.layout')

  @section('content')
    <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="index.php" class="brand-login">
                                <img src="img/logo.svg" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Register with us</h1>                            
                        </div>
                        <!-- form -->
                  
                        <form method="POST" class="form py-3" action="{{ route('register') }}">
                        @csrf
                            <div class="form-group">
                                <label for="userNameInput">Enter your Name</label>
                                <div class="input-group">
                                    <input type="text" required="" class="form-control @error('name') is-invalid @enderror" name="user_name"  id="userNameInput" placeholder="Write Name">
                                </div>
                                @error('user_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Email</label>
                                <div class="input-group">
                                    <input type="email" required="" class="form-control @error('email') is-invalid @enderror"  name="user_email" id="userNameInput" placeholder="Write Email">
                                </div>
                                @error('user_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" required="" class="form-control" name="user_mobile_no" id="userNameInput" placeholder="Mobile Number">
                                </div>
                                @error('user_mobile_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Create Password</label>
                                <div class="input-group">
                                    <input type="password" required="" class="form-control @error('password') is-invalid @enderror" name="password" id="userNameInput" placeholder="Password">
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                               <label for="userNameInput">Create Password</label>
                                <div class="input-group">
                                    <input type="password"  required="" class="form-control" autocomplete="new-password" id="userNameInput" placeholder="Confirm Password">
                                </div>
                            </div>                            
                            <input type="submit" class="btn orange-btn w-100 mt-2" value="Signup">
                            <p class="text-center">
                                Already have an account? <a class="forange" href="login.php">Sigin</a>
                            </p>
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 loginrt d-none d-lg-block"></div>
                <!--/ col -->
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>
   
  @stop 
    
    </body>
</html>