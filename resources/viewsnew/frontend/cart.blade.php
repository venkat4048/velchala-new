<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Cart Items ({{ count($cart_detailes) }})</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Cart Items</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body py-4">

            <!--container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-8 wow animate__animated animate__fadeInUp">
                        <!-- row -->
                        <div class="table-headerrow row">
                            <!-- col -->
                            <div class="col-md-6">
                               <p>Product Details</p>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2">
                                <p>Qty</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Price</p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Total</p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        @foreach($cart_detailes as $value)

                        <div class="row cartlist-itemrow py-2">
                            <!-- col -->
                            <div class="col-md-6 col-12">
                               <div class="d-flex">
                                    <a href="publication-detail.php" class="d-block cartimteimg">
                                        <img src="{{ url('theme/publications').'/'.$value->img }}" alt="" class="w-100">
                                    </a>
                                    <div class="cart-item-details">
                                        <h6 class="h6 pb-2 fsbold">
                                            <a href="publication-detail" class="fgray">{{ $value->publication_name }}</a>
                                        </h6>
                                        <p class="pb-0"> Language: <span>{{ $value->language }}</span></p>
                                        <p> Product Code: <span>{{ $value->sku_code }}</span></p>

                                        <p>
                                            <a href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                               </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <p>
                                    <input type="number" class="form-control" value="{{ $value->quanity }}">
                                </p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single"> {{ $value->dis_price }}  </p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single fsbold forange"> {{ $value->dis_price*$value->quanity }}  </p>
                             </div>
                            <!--/ col -->
                        </div>
                        @endforeach
                        <!--/ row -->

                        <p class="pt-2">
                            <a href="{{ url('publications')}}" class="fblue"><span class="icon-arrows"></span> Continue Shopping</a>
                        </p>

                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-lg-4 wow animate__animated animate__fadeInDown">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS {{ count($cart_detailes) }}</span>
                                    <span class="price-single">{{ $value->total_price }}</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">{{ $value->total_price+150 }}</span>
                                 </p>
                                 <p>
                                     <a href="{{ url('checkout')}}" class="orange-btn w-100 text-center">Checkout</a>
                                 </p>
                             </div>
                         </div>
                         <!--/ card -->
                     </div>
                    <!--/ left col -->


                </div>
                <!--/ row -->
            </div>
            <!--/ container -->           
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
   
    @stop
    </body>
</html>