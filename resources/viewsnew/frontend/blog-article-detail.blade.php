<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
      @extends('includes.blog_layout')

  @section('content')
    <!--main-->   
    <main class="blog-main">

    <!-- container -->
    <div class="container">
        <!-- card -->
        <div class="card blog-detail">
            <!-- card header -->
            <div class="card-header">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__fadeInUp">
                    <div class="col-lg-8">
                        <div class="d-flex justify-content-between pb-3">
                            <a href="{{ url('blog-article') }}" class="fblue"><span class="icon-arrows"></span> Back to Articles</a>
                            <a href="{{ url('theme/blog_article').'/'.$blog_articles->blog_article_pdf }}" download class="">Download <span class="icon-cloud-download icomoon"></span> </a>
                        </div>
                        <h1 class="h1 pb-3">{{ $blog_articles->blog_article_name }}</h1>
                        <p class="pb-0 mb-0">Posted on <span class="fsbold">25-06-2020</span></p>

                         <!-- social share -->
                        <div class="social-share py-3 d-flex">
                            <span class="pt-1 span-share">Share this Article</span>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-pinterest-square icomoon"></span></a>
                        </div>
                        <!--/ social share -->
                        <p></p>
                    </div>
                    
                </div>
                <!--/ row -->
            </div>
            <!-- card header -->

             <!-- card body -->
             <div class="card-body">              

                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <!-- responsive tab -->
                    <div class="">
                        <!--<h4>Me unpleasing impossible</h4>-->
                        <p>{{ $blog_articles->blog_article_desc }}</p>
                    </div>
                    <!--/ responsive tab -->
                       
                    </div>
                </div>
                <!--/ row -->
             </div>
            <!-- card body -->


        </div>
        <!--/card -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    @stop
    </body>
</html>