<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Video Gallery</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>Videos</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                  @if(count($video)>0)
                  @foreach($video as $value) 
                 <!-- col -->
                  <div class="col-sm-6 col-md-4 text-center video-col wow animate__animated animate__fadeInDown">
                    <iframe width="100%"  src="https://www.youtube.com/embed/{{ $value->video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p class="text-left">HMTV Special Focus On Sahitya Sangamam Rao </p>
                </div>
                 @endforeach
                 @else
                 <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif    
                <!--/ col -->

                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
       </div>
       <!--/ sub page body -->
       
    </main> 
    <!--/ main-->
   @stop   
    </body>
</html>