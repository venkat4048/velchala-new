<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.blog_layout')

  @section('content')
    <!--main-->   
    <main class="blog-main">

    <!-- container -->
    <div class="container">
       <!-- row -->
       <div class="row justify-content-center">
           <!-- card -->
          @if(count($blog_articles)>0)
          @foreach($blog_articles as $value)
            <div class="col-lg-4">
                <div class="card blogcard articlecard">
                    <div class="card-header">	
                       {{ $value->blog_article_name }}
                        <span class="badge badge-success">New</span>
                    </div>
                    <div class="card-body">
                    <p> {{ substr("$value->blog_article_desc", 0,20) }}..</p>
                    <a href="{{ url('blog-article-detail',['url'=>$value->blog_article_url]) }}" class="btn orange-btn">Read More</a>
                    </div>
                </div>              
            </div>
          @endforeach
          @else 
           <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            </div>
            @endif   
           
            <!--/ card -->           

       </div>
       <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    @stop
    </body>
</html>