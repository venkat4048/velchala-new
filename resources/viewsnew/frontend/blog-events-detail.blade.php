<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
<meta property="og:url" content="{{ url('jayanthi-events-details',['url'=>$blogs->blog_url]) }}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ $blogs->blog_name }}" /> 
<meta property="og:description" content="{{ $blogs->blog_name }}" /> 
<meta property="og:image" content="{{ url('theme/uploads/blogs').'/'.$blogs->blog_img }}" />
     @extends('includes.blog_layout')

  @section('content')
    <!--main-->   
    <main class="blog-main">

    <!-- container -->
    <div class="container">
        <!-- card -->
        <div class="card my-3 blog-detail">
            <!-- card header -->
            <div class="card-header">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__fadeInUp">
                    <div class="col-lg-8">
                        <a href="{{ url('blog-events') }}" class="fblue"><span class="icon-arrows"></span> Back to Events</a>
                        <h1 class="h1 pb-3">{{ $blogs->blog_name }}</h1>
                        <p class="pb-0 mb-0">Hyderabad <span class="d-inline-block px-3 small pb-3">|</span>25-06-2020</p>

                         <!-- social share -->
                        <div class="social-share py-3 d-flex">
                            <span class="pt-1 span-share">Share this product</span>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('jayanthi-events-details',['url'=>$blogs->blog_url]) }}" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                        </div>
                        <!--/ social share -->
                    </div>
                    
                </div>
                <!--/ row -->
            </div>
            <!-- card header -->

             <!-- card body -->
             <div class="card-body">
                <img class="card-img-top img-fluid mb-3" src="{{ url('theme/uploads/blogs').'/'.$blogs->blog_img }}">

                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab wow animate__animated animate__fadeInUp">
                            <ul class="resp-tabs-list hor_1">
                                <li>Description</li>
                                <li>Photos</li>
                                <li>Videos</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <!-- description -->
                                <div>
                                <p>{{ $blogs->blog_desc }}</p>
                                </div>
                                <!--/ description -->

                                <!-- reviews -->
                                <div>
                                     @if(count($photos)!="")
                                    <h2 class="h4 ptregular pb-3 border-bottom">Event Photo Gallery <span>({{count($photos)}})
                                    </span></h2>
                                 
                            
                              @endif
                                    <div class="gallery-block grid-gallery">
                                        <!-- row -->
                                        <div class="row">
                                            <!-- item -->
                                          @if(count($photos)>0)
                                          @foreach($photos as $value)
                                            <div class="col-6 col-sm-6 col-md-3 item">
                                                <a class="lightbox" href="{{ url('theme/uploads/photos').'/'.$value->img }}">
                                                    <img class="img-fluid image scale-on-hover" src="{{ url('theme/uploads/photos').'/'.$value->img }}">
                                                </a>
                                            </div>
                                           @endforeach
                                             @else 
                                    <div class="row justify-content-center no-data">
                                <div class="col-md-6 text-center">
                                    <h2 class="h2">No Data Available Now</h2>
                                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                    <p>Thank you for visit us</p>
                                </div>
                            </div>
                              @endif  
                                            <!-- item -->
                                            
                                            <!-- item -->                                          
                                    </div>
                                    <!--/ row -->
                                </div>
                                   

                                    

                                </div>
                                <!--/ reviews -->

                                <!-- related videos -->
                                <div>
                              @if(count($video)!="")
                                    <h3 class="h4 ptregular pb-3 border-bottom">Related videos <span>({{count($video)}})</span></h3>
                               @else 
                            <div class="row justify-content-center no-data">
                                <div class="col-md-6 text-center">
                                    <h2 class="h2">No Data Available Now</h2>
                                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                    <p>Thank you for visit us</p>
                                </div>
                            </div>
                              @endif
                                 

                                     <!-- row -->
                                     <div class="row justify-content-center py-3">
                                        @foreach($video as $value)
                                         <!-- col -->
                                         <div class="col-md-4 text-center video-col">
                                         <iframe width="100%" src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p class="text-left">HMTV Special Focus On Sahitya Sangamam  Part</p>
                                        </div>
                                        <!--/ col -->
                                        @endforeach  
                                    </div>
                                    <!--/row -->


                                </div>
                                <!--/ related videos -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->
                       
                    </div>
                </div>
                <!--/ row -->
             </div>
            <!-- card body -->


        </div>
        <!--/card -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

     @stop
    </body>
</html>