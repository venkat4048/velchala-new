<!-- //style -->
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<!--<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">-->

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<!--<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">-->
<!-- //style -->


 <!-- header -->
 <header class="blog-header fixed-top">
        <!-- cust container -->
        <div class="container">
            <div class="navbar navbar-expand-lg bsnav px-0">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{ url('theme/img/logo.svg') }}" alt="">
            </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-md-end">
                    <ul class="navbar-nav navbar-mobile mr-0">
                    <li class="nav-item"><a class="nav-link" href="{{url('/')}}">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog-article')}}">Articles</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog_news')}}">News</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog_interviews')}}">Interviews</a></li>
                    <li class="nav-item active"><a class="nav-link" href="{{url('/blog-events')}}">Events</a></li>
                    </ul>
                </div>
            </div> 
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>   
        </div>
        <!--/ cust container -->
    </header>
    <!--/ header -->


     

@yield('content')






    <!-- footer -->
    <footer class="footer-blog">
        <!-- container -->
        <div class="container">

            <h3 class="h3 text-center fsbold">Stay with us</h3>            
            <p class="text-center py-2">Subscribe to my newsletter for all the latest updates </p>

            <div class="subscribe">
                <div class="form-group d-flex">
                    <input type="text" class="form-control" placeholder="Write Your Email">
                    <button class="btn blue-btn">Subscribe</button>
                </div>
            </div>
            
            <ul class="nav justify-content-center socialnav aos-item">
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-twitter icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-linkedin icomoon"></span></a>
                </li>
            </ul>            
        </div>
        <!--/container -->
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>
    <!--/ footer -->
    <!--/ footer -->

  <!-- scripts for jquery,  bootstrap and custom script files -->
<script src="{{ url('theme/js/jquery-3.2.1.min.js')}}"></script>   
<script src="{{ url('theme/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ url('theme/js/popper.js')}}"></script>     
<!--  navigation -->     
<script src="{{ url('theme/js/bsnav.js')}}"></script>   

<!-- animate wowjs-->
<script src="{{ url('theme/js/wow.min.js')}}"></script>

<!-- file upload  -->
<script src="{{ url('theme/js/yt-video-background.min.js')}}" charset="utf-8"></script>

<!-- typing effect -->
<script src="{{ url('theme/js/typingEffect.js')}}"></script>
<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![end if ]-->
<script src="{{ url('theme/js/swiper.min.js')}}"></script>

<!-- script files for grid gallery -->
<script src="{{ url('theme/js/baguetteBox.js')}}"></script>    

<!-- responsive tab -->
<script src="{{ url('theme/js/easyResponsiveTabs.js')}}"></script>

<!-- custom script -->
<script src="{{ url('theme/js/custom.js')}}"></script>  

<!-- accordion -->
<script src="{{ url('theme/js/accordion.js')}}"></script>

<!--/ stepper -->
<script src="{{ url('theme/js/stepper.js')}}"></script>

<!-- file upload  -->
<script src="{{ url('theme/js/imageuploadify.min.js')}}"></script>

<!-- fade loading  -->
<script src="{{ url('theme/js/animsition.js')}}"></script>
<script>
  $(document).ready(function() {
    $('.animsition').animsition();
  });
  </script>
    <!-- hero slider -->
    <script src="{{ url('theme/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{ url('theme/js/owl.carousel.min.js')}}"></script> 
    <script src="{{ url('theme/js/jquery.nicescroll.min.js')}}"></script> 
    <script src="{{ url('theme/js/main.js')}}" ></script>   

    <!-- book popup --> 
    <div class="modal fade" id="book-more" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Viswanatha A Literary legend</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- modal content -->
                <div class="modal-body">
                <!-- row -->
                <div class="row justify-content-around">
                    <!-- col -->
                    <div class="col-lg-5 text-center">
                        <img src="{{ url('theme/img/coverpages/cover01.jpg" class="img-fluid" alt="">
                    </div>
                    <!--/col-->
                        <!-- col -->
                        <div class="col-lg-7">
                            <article class="book-pop-article">
                                <h6 class="h6 border-bottom d-flex justify-content-between">
                                    <span>Description</span>
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist" >
                                        <span class="icon-heart"></span>
                                    </a>
                                </h6>
                                <p class="pb-0 mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus asperiores earum suscipit. Dignissimos laudantium quasi eius, unde qui consequatur </p>
                            </article>
                            <dl class="list-dl">
                                <dt>Price:</dt>
                                <dd>
                                    <p class="price">
                                        <span class="offer">275</span>
                                        <span class="oldprice">325</span>
                                    </p>
                                </dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Author:</dt>
                                <dd>Velchala Kondal Rao</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Publisher:</dt>
                                <dd>Velchala Publications</dd>                           
                            </dl>                      
                            <dl class="list-dl">
                                <dt>No of Pages:</dt>
                                <dd>278</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Language:</dt>
                                <dd>English</dd>                           
                            </dl>

                            <!--buttons -->
                            <div class="pt-2">
                                <a href="javascript:void(0)" class="orange-btn">Read Online</a>
                                <a href="cart.php" class="orange-btn">Add to Cart</a> 
                            </div>
                            <!--buttons -->
                        </div>
                        <!--/col-->
                </div>
                <!--/ row -->
                </div>    
                <!--/ modal content -->    
            </div>
        </div>
    </div>
    <!--/ book popup-->
 
 <script>
    //add class to header on scroll
    $(window).scroll(function(){
        if($(this).scrollTop() > 50){
            $('.ashokaChakra').addClass('DynamicChakra');
            
        }else{
            $('.ashokaChakra').removeClass('DynamicChakra');
        }
    });
 </script>
  <script>
    $(document).ready(function(){        
        $('#wishlist-icon').click(function(){
            $('#alertAddWishlist-Detail').show();
        });

        $('#addtokcart-icon').click(function(){
          
            $('#alertAddtocart').show();
        }) 
    });
   </script>