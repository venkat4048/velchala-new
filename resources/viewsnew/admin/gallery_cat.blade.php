@extends('admin.includes.styles')
@section('content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center py-3">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Gallery Catergories</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/categories')}}">Home</a></li>
                    <li><span>Gallery Catergories</span></li>
                </ul>
            </div>
        </div>                    
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">

<!-- table card -->
<div class="card">
    <div class="card-body">
       <div class="d-flex justify-content-between">
            <h4 class="header-title">List of Gallery Catergories</h4>
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Gallery Catergories</button>
       </div>
       @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="text-uppercase">
                        <tr>
                            <th scope="col">S.No:</th>
                            <th scope="col">Category Name</th> 
                            <th scope="col">Action</th> 

                           
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($gallery_cat as $value) 
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ ucfirst($value->gallery_cat_name) }}</td>                                        
                            <td>
                                <a @if($value->gallery_cat_name=='Poems') href="{{url('/view-gallery')}}" @endif
                                   @if($value->gallery_cat_name=='Videos') href="{{url('/view-gallery-video')}}" @endif
                                   @if($value->gallery_cat_name=='Photos')
                                   href="{{url('/admin-events')}}" @endif
                                    >
                                  <button type="button" class="btn btn-success mb-3" ><i class="fa fa-plus-square"></i> View</button>
                                </a>
                            </td> 
                            
                        </tr>      
                       @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--/ table -->
</div>
@endsection

   
    
  <!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <form  action = "{{url('/gallery-cat-create')}}" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Gallery Catergory</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Gallery Catergory Name</label>
                        <input required="" name="gallery_cat_name" placeholder="Gallery Catergory Name" class="form-control" type="text" value="" id="example-text-input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!--/ new category modal -->
