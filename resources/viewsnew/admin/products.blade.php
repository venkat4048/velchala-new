@extends('admin.includes.styles')
@section('content')
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Books</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><span>Books</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Books</h4>
                        <a href="{{url('/publications-create')}}"  class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> New Book</a>
                   </div>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Sno</th>
                                    <th>Publishers Name</th>
                                    <th>Publications Name</th>
                                    <th>Discount</th>
                                    <th>Date</th>
                                    <th>Edit</th>
                                    <th>Publish</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($publications as $value)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$value->getCategory->pub_name}}</td>
                                    <td><a href="{{ url('publications_view',['publications_view'=>$value->publication_slug]) }}">{{ucfirst($value->publication_name)}}</a></td>
                                    <td>{{$value->dis_price}}</td>
                                    <td>{{date("d-m-Y",strtotime($value->published_date))}}</td>
                                    <td><a href="{{ url('publications_edit',['publications_id'=>$value->id]) }}">Edit</a></td>
                                    @if($value->publications_status==1)
                                    <td><a onclick="return confirm();" href="{{url('/book_publish',['publications_id'=>$value->id])}}">Un Publish</a></td>
                                    @else
                                    <td><a onclick="return confirm123('Are you sure want to Change Status');" href="{{url('/book_publish',['publications_id'=>$value->id])}}"><i class="fa fa-trash"></i>Publish</a></td>
                                    @endif
                                    <td><a onclick="return confirm_delete()" href="{{url('publications_delete',['publications_id'=>$value->id])}}">Delete</a></td>
                                </tr>
                               @endforeach  
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
             <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
           
            <!--/ table data -->
            </div>
           @endsection
