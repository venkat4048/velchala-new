@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/blogs-update')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Edit BLogs</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_BLogs')}}">BLogs</a></li>
                                <li><span>Edit BLogs</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button  type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save BLogs</button>
                        <a  href="{{url('/blogs')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                              
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-12">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                      
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog Name</label>
                                <input class="form-control" type="text" name="blog_name" placeholder="BLog Name" id="example-text-input" value="{{$blogs->blog_name}}">
                                <input type="hidden" name="id" value="{{$blogs->id}}">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog Image</label>
                                <input  class="form-control" type="file" name="blog_image" placeholder="BLog Image" id="example-text-input">
                                <input type="hidden" name="old_img" value="{{$blogs->blog_img}}">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                         
                        <!--/ col -->
                        
                         <!-- col -->
                         
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Title</label>
                                <input required="" class="form-control" type="text" name="meta_title" placeholder="Meta Title" id="example-text-input" value="{{$blogs->meta_title}}">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Keywords</label>
                                <input required="" class="form-control" type="text" name="meta_keywords" placeholder="Meta Keywords" id="example-text-input" value="{{$blogs->meta_keywords}}">
                            </div>
                         </div>
                        <!--/ col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Description</label>
                                <input required="" class="form-control" type="text" name="meta_description" placeholder="Meta Description" id="example-text-input" value="{{$blogs->meta_description}}">
                            </div>
                         </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Description</label>
                                <div class="page-wrapper box-content">
                                <textarea required="" class="form-control content" type="text" name="blog_description" placeholder="Blog Description" id="example-text-input">{{$blogs->blog_desc}}</textarea>
                            </div>
                         </div>
                         <!-- col -->
                       
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
          
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
