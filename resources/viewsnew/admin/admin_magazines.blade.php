@extends('admin.includes.styles')
@section('content')
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">List of Magazines</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><span>List of Magazines</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Magazines</h4>
                        <a href="{{url('/magazine_add')}}"  class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> Magazines</a>
                   </div>
                    <div class="data-tables">
                       <table class="table table-bordered text-center">
                              <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Sno</th>
                                    
                                    <th scope="col">Magazine Cover Page</th>
                                    <th scope="col">Magazines Date</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($magazines as $value)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                   
                                    <td><img href="{{ url('admin_blog_article_view',['blog_art_id'=>$value->id]) }}"></td>
                                   
                                    <td>{{  date("d-m-Y",strtotime($value->mag_date)) }}</td>
                                    <td>
                                       <a href="{{ url('magazines-edit',['event_id'=>$value->id]) }}">
                                          <button type="button" class="btn btn-success mb-3"  ><i class="fa fa-plus-square"></i> Edit</button>
                                        </a>
                                    </td>
                                    <td><a onclick="return confirm_delete();" href="{{ url('magazines-delete',['event_id'=>$value->id]) }}"><i class="fa fa-trash"></i>Delete</a></td>
                                </tr>
                               @endforeach  
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
            <!--/ table data -->
            </div>
                <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
           @endsection
