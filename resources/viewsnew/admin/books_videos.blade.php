@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            
              
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">View {{  $publications_details->publication_name }}</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_publications')}}">Publications</a></li>
                                <li><span>New {{  $publications_details->publication_name }}</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        
                        <a  href="{{url('/admin_publications')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>         
            <!-- container fluid -->
            <div class="container-fluid">
                <!-- tab -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link" id="nav-home-tab"  href="{{ url('publications_view',['publications_view'=>$publications_details->publication_slug]) }}" role="tab" aria-controls="nav-home" aria-selected="true">View Book Details</a>
                        <a class="nav-item nav-link active" id="nav-profile-tab"  href="{{ url('admin_books_videos',['publications_id'=>$publications_details->publi_id])}}" role="tab" aria-controls="nav-profile" aria-selected="false">Videos</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews</a>
                    </div>
                </nav>
                    <div class="tab-content" id="nav-tabContent">
                    <!-- tab 1-->
                    <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <!-- page title area start -->
            
                    </div>
                    <!-- tab 01-->
                    <!-- tabv 2-->
                    <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                       <div class="card">
    <div class="card-body">
       <div class="d-flex justify-content-between">
            <h4 class="header-title">List of Videos </h4>
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Video </button>
       </div>
       @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="text-uppercase">
                        <tr>
                            <th scope="col">S.No:</th>
                            <th scope="col"> Image</th> 
                            <th scope="col">Action</th> 

                           
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($video as $value) 
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                                                                 
                        
                                <td><iframe width="100px" height="100px" src="https://www.youtube.com/embed/{{ $value->video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></td>
                                <td>
                                <a href="{{ url('gallery-video-delete',['id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-danger mb-3" onclick="return confirm_delete();" ><i class="fa fa-plus-square"></i> Delete</button>
                                </a>
                                  </td>                                
                                                                   
                            
                           
                        </tr>      
                       @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <form  action = "{{url('/gallery-video-create',['book_id'=>$publications_details->publi_id,'cat_id'=>2])}}"  enctype="multipart/form-data" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Video </h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                

                <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Video Url</label>
                        <input required="" name="video_url" placeholder="Video Url" class="form-control" type="text"  value="" id="example-text-input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </div>
        </form>
    </div>
    <!--/ new category modal -->
                    </div>
                    <!--/ tab 2-->
                    <!-- tab 03-->
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <h1>Tab 03 content </h1>
                    </div>
                    <!--/ tab 03-->
                </div>
                <!--/ tab -->
            </div>
            <!--/ container fluid -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
