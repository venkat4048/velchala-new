<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Biosyn Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
      <?php include 'sidebar.php' ?>
        <!-- main content area start -->
        <div class="main-content">
           <?php include 'header.php' ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Settings</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="dashboard.php">Home</a></li>
                                <li><span>Settings</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <!-- row -->
                <div class="row mt-5">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- card -->
                        <div class="card">
                           
                            <div class="card-body">
                            <dl>
                                <dt>Company Name:</dt>
                                <dd>Bio Syn Chemicals pvt Ltd</dd>
                            </dl>
                            <dl>
                                <dt>Contact Number:</dt>
                                <dd>+844 1800 33 555</dd>
                            </dl>
                            <dl>
                                <dt>Email:</dt>
                                <dd>info@biosyn.in</dd>
                            </dl>
                            <dl>
                                <dt>Username:</dt>
                                <dd>biosynadmin</dd>
                            </dl>
                            <dl>
                                <dt>Password:</dt>
                                <dd>*********</dd>
                            </dl>   
                            <button data-toggle="modal" data-target="#editsettings" class="btn btn-success" type="button">Edit</button>                        
                            </div>
                            
                        </div>
                        <!--/ card -->
                       
                    </div>
                    <!-- col -->
                </div>
                <!--/ row -->


           
            </div>
            <!-- main content area end -->
            <?php include 'footer.php' ?>
    </div>
    <!-- page container area end -->

     <!-- Modal -->
     <div class="modal fade" id="editsettings" data-backdrop="static" data-keyboard="false">
         <!-- new category modal -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Settings</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <!-- details of enquiry -->
                   <form>
                       <!-- row -->
                       <div class="row">
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Company Name</label>
                                <input class="form-control" type="text" placeholder="Company Name">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Contact Number</label>
                                <input class="form-control" type="text" placeholder="9642123254">
                            </div>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Email</label>
                                <input class="form-control" type="text" placeholder="Enter Email">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Username</label>
                                <input class="form-control" type="text" placeholder="Username">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Password</label>
                                <input class="form-control" type="password" placeholder="Password">
                                <label>Password Name will be here</label>
                            </div>
                         </div>
                        <!--/ col -->
                       </div>
                       <!--/ row -->
                   </form>
                    <!--/ detail of enquiry -->
                </div>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>             
            </div>
        </div>
    </div>
    <!--/ new category modal -->
    

    <?php include 'scripts.php' ?>
</body>

</html>