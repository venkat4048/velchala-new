@extends('admin.includes.styles')
@section('content')
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Blog News</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><span>Blogs</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Blog News</h4>
                        <a href="{{url('/blogs-news-add')}}"  class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> New Blog</a>
                   </div>
                    <div class="data-tables">
                        <table class="table table-bordered text-center">
                              <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Sno</th>
                                    <th scope="col">Blog News Name</th>
                                    <th scope="col">Blog News Post Date</th>
                                    <th scope="col">Blog News Paper Name</th>
                                   
                                    
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blog_news as $value)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</td>
                                   
                                    <td><a href="{{ url('blog_view',['blog_id'=>$value->id]) }}">{{ucfirst($value->blog_news_name)}}</a></td>
                                    <td>{{  date("d-m-Y",strtotime($value->created_at)) }}</td>
                                    <td>{{ucfirst($value->blog_news_paper_name)}}</td>
                                   
                                    
                                    <td>
                                       <a href="{{ url('admin-blog-news-edit',['event_id'=>$value->id]) }}">
                                          <button type="button" class="btn btn-success mb-3"  ><i class="fa fa-plus-square"></i> Edit</button>
                                        </a>
                                    </td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i>Delete</a></td>
                                </tr>
                               @endforeach  
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
            <!--/ table data -->
            </div>
           @endsection
