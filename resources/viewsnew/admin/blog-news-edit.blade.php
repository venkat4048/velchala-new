@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/blogs-news-update')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Edit BLogs News</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_BLogs')}}">BLogs News</a></li>
                                <li><span>Edit BLogs News</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button  type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save BLog News</button>
                        <a  href="{{url('/admin_BLogs')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                              
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-12">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                      
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog News Name</label>
                                <input class="form-control" type="text" name="blog_news_name" placeholder="BLog News Name" id="example-text-input" value="{{$blog_news->blog_news_name}}">
                                 <input type="hidden" name="id" value="{{$blog_news->id}}">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog News Imgs</label>
                                <input class="form-control" type="file" name="blog_news_img" placeholder="BLog Image" id="example-text-input">
                                   <input type="hidden" name="old_img" value="{{$blog_news->blog_news_img}}">
                            </div>
                         </div>
                        <!--/ col -->
                      
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog News Paper Name</label>
                                <input required="" class="form-control" type="text" name="blog_news_paper_name" placeholder="Blog News Paper Name" id="example-text-input" value="{{$blog_news->blog_news_paper_name}}">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blogs News Post Date</label>
                                <input required="" class="form-control" type="date" name="blog_news_date" placeholder="Meta Keywords" id="example-text-input" value="{{$blog_news->blog_news_date}}">
                            </div>
                         </div>
                        <!--/ col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blogs News Url</label>
                                <input required="" class="form-control" type="text" name="blog_news_url" placeholder="Meta Description" id="example-text-input" value="{{$blog_news->blog_news_url}}">
                            </div>
                         </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog News Description</label>
                                <div class="page-wrapper box-content">
                                <textarea required="" class="form-control content" type="text" name="blog_news_des" placeholder="Blog Description" id="example-text-input">{{$blog_news->blog_news_des}}</textarea>
                            </div>
                            </div>
                         </div>
                         <!-- col -->
                       
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
          
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
