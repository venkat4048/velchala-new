<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Velchala Kondal Rao</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="{{ url('theme/admin/assets/images/icon/favicon.ico')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="t{{ url('heme/admin/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/metisMenu.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/slicknav.min.css')}}">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <!-- others css -->
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/typography.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/default-css.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{ url('theme/admin/assets/css/responsive.css')}}">
    <!-- modernizr css -->
    <script src="{{ url('theme/admin/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
    <!-----side menu ------------------------>
      <!-- sidebar menu area start -->
  <div class="sidebar-menu">
<div class="sidebar-header">
    <div class="logo">
        <a href="{{url('/dashboard')}}"><img src="{{ url('theme/img/logo.svg')}}" alt="logo"></a>
    </div>
</div>
<div class="main-menu">
    <div class="menu-inner">
        <nav>
            <ul class="metismenu" id="menu">
                <li class="active"><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="{{url('/categories')}}"><i class="fa fa-list"></i> <span>Publishers</span></a></li>
                <li><a href="{{route('admin_books')}}"><i class="fa fa-hourglass"></i> <span>Publications</span></a></li>
                <li><a href="{{url('/gallery-cat-admin')}}"><i class="fa fa-hourglass"></i> <span>Gallery Category</span></a></li>
                <li><a href="{{url('/blogs')}}"><i class="fa fa-hourglass"></i> <span>Blogs</span></a></li>
                <li><a href="{{url('/admin-blog-articles')}}"><i class="fa fa-hourglass"></i> <span>Blogs Articles</span></a></li>
                <li><a href="{{url('/admin-blog-news')}}"><i class="fa fa-hourglass"></i> <span>Blog News</span></a></li>
                <li><a href="{{url('/blog-articles')}}"><i class="fa fa-hourglass"></i> <span>Blogs Articles</span></a></li>
                <li><a href="{{url('/admin-blog-interviews')}}"><i class="fa fa-hourglass"></i> <span>Blog Interviews</span></a></li>
                <li><a href="{{url('/enquiries')}}"><i class="fa fa-headphones"></i> <span>Enquiries</span></a></li>
                <li><a href="{{url('/users')}}"><i class="fa fa-hourglass"></i> <span>Users</span></a></li>
                <li><a href="{{url('/admin-events')}}"><i class="fa fa-hourglass"></i> <span>Events</span></a></li>
                <li><a href="{{url('/subscribes')}}"><i class="fa fa-hourglass"></i> <span>Subscribes</span></a></li>
                <li><a href="{{url('/contact-admin')}}"><i class="fa fa-hourglass"></i> <span>contact</span></a></li>
                <li><a href="{{url('/settings')}}"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
                <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
            </ul>
        </nav>
    </div>
</div>
</div>
<!-- sidebar menu area end -->
 <div class="main-content">

 <!-- header area start -->
 <div class="header-area">
<div class="row align-items-center">
    <!-- nav and search button -->
    <div class="col-md-6 col-sm-8 clearfix">
        <div class="nav-btn pull-left">
            <span></span>
            <span></span>
            <span></span>
        </div>                      
    </div>
    <!-- profile info & task notification -->
    <div class="col-md-6 col-sm-4 clearfix">
        <ul class="notification-area pull-right">
            <li id="full-view"><i class="ti-fullscreen"></i></li>
            <li id="full-view-exit"><i class="ti-zoom-out"></i></li> 
        </ul>
    </div>
</div>
</div>
<!-- header area end -->

@yield('content')
<!------------ footer --------->

 <!-- footer area start-->
 <footer>
    <div class="footer-area">
        <p>© Copyright 2020. All right reserved by <a href="{{url('/')}}">Velchala Kondal Rao</a>.</p>
    </div>
</footer>
<!-- footer area end-->

</div>
<!-- page container area end -->

 <!-- jquery latest version -->
 <script src="{{ url('theme/admin/assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ url('theme/admin/assets/js/popper.min.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/metisMenu.min.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/jquery.slicknav.min.js')}}"></script>

     <!-- Start datatable js -->
     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

    <!-- start chart js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <!-- start highcharts js -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <!-- start zingchart js -->
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
    </script>
    <!-- all line chart activation -->
    <script src="{{ url('theme/admin/assets/js/line-chart.js')}}"></script>
    <!-- all bar chart activation -->
    <script src="{{ url('theme/admin/assets/js/bar-chart.js')}}"></script>
    <!-- all pie chart -->
    <script src="{{ url('theme/admin/assets/js/pie-chart.js')}}"></script>
    <!-- others plugins -->
    <script src="{{ url('theme/admin/assets/js/plugins.js')}}"></script>
    <script src="{{ url('theme/admin/assets/js/scripts.js')}}"></script>
   

</body>

</html>