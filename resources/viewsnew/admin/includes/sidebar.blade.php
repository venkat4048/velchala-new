  <!-- sidebar menu area start -->
<div class="sidebar-menu">
<div class="sidebar-header">
    <div class="logo">
        <a href="{{url('/dashboard')}}"><img src="theme/img/logo.svg" alt="logo"></a>
    </div>
</div>
<div class="main-menu">
    <div class="menu-inner">
        <nav>
            <ul class="metismenu" id="menu">
                <li class="active"><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="{{url('/categories')}}"><i class="fa fa-list"></i> <span>Publishers</span></a></li>
                <li><a href="{{url('/products')}}"><i class="fa fa-hourglass"></i> <span>Publications</span></a></li>
                 <li><a href="{{url('/gallery-cat-admin')}}"><i class="fa fa-hourglass"></i> <span>Publications</span></a></li>
                <li><a href="{{url('/enquiries')}}"><i class="fa fa-headphones"></i> <span>Enquiries</span></a></li>
                <li><a href="{{url('/settings')}}"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
                <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
            </ul>
        </nav>
    </div>
</div>
</div>
<!-- sidebar menu area end -->