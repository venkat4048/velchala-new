@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/magazine_store')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Magazines</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_BLogs')}}">Magazines</a></li>
                                <li><span>Magazines</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button  type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save Magazine</button>
                        <a  href="{{url('/admin_magazine')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                              
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-12">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                      
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Magazine Profile Pic</label>
                                <input required="" class="form-control" type="file" name="mag_profile_pic" placeholder="BLog Image" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Magazines Pdf</label>
                                <input required="" class="form-control" type="file" name="mag_pdf" placeholder="BLog Image" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                         
                        <!--/ col -->
                        
                         <!-- col -->
                         
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Date</label>
                                <input required="" class="form-control" type="date" name="mag_date" placeholder="Blog News Paper Name" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          
                        <!--/ col -->
                         
                         <!-- col -->
                       
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
          
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
