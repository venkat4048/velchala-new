@extends('admin.includes.styles')
@section('content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center py-3">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Events </h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/categories')}}">Home</a></li>
                    <li><span>Events </span></li>
                </ul>
            </div>
        </div>                    
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">

<!-- table card -->
<div class="card">
    <div class="card-body">
       <div class="d-flex justify-content-between">
            <h4 class="header-title">List of Events </h4>
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Events </button>
       </div>
       @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
     @endif
        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="text-uppercase">
                        <tr>
                            <th scope="col">S.No:</th>
                            <th scope="col">Event Name</th>
                            <th scope="col">Event Image</th> 
                            <th scope="col">Event Date</th> 
                            <th scope="col">Event Locations</th> 
                            <th scope="col">Action</th> 
                            <th scope="col">Action</th> 
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($events as $value) 
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $value->event_name }}</td>
                            <td><img src="theme/uploads/events/{{ $value->img }}" alt="" class="img-fluid" height="100px" width="100px"></td> 
                            <td>{{ $value->event_date }}</td> 
                            <td>{{ $value->event_location }}</td>     
                            <td>
                                <a href="{{ url('events-edit',['event_id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-success mb-3"  ><i class="fa fa-plus-square"></i> Edit</button>
                                </a>
                            </td>   
                             <td>
                                <a href="{{ url('admin-events-view',['event_id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-success mb-3"  ><i class="fa fa-plus-square"></i> View</button>
                                </a>/<a href="{{ url('admin-events-delete',['id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-danger mb-3" onclick="return confirm_delete();" ><i class="fa fa-plus-square"></i> Delete</button>
                                </a>
                            </td>                                     
                        </tr>      
                       @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--/ table -->
</div>
@endsection

   
    
  <!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <form  action = "{{url('/admin-events-create')}}"  enctype="multipart/form-data" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Events </h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                   <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Event Name</label>
                        <input required="" name="event_name" placeholder="Event Name" class="form-control" type="text"  value="" id="example-text-input">
                    </div>
                    
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Event Location</label>
                        <input required="" name="event_location" placeholder="Event Name" class="form-control" type="text"  value="" id="example-text-input">
                    </div>

                
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Event Image</label>
                        <input required="" name="event_img" placeholder="Events Image" class="form-control" type="file"  value="" id="example-text-input">
                    </div>
               

                  
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Event Date</label>
                        <input required="" name="event_date" placeholder="Event Date" class="form-control" type="date"  value="" id="example-text-input">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Event Description</label>
                        <textarea required="" name="event_des" placeholder="Event Description" class="form-control" type="date"  value="" id="example-text-input"></textarea>
                    </div>
                </div>
               
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </div>
        </form>
    </div>
    <!--/ new category modal -->
        <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
