@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/blogs-article-insert')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">New BLogs Articles</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_BLogs')}}">BLogs Articles</a></li>
                                <li><span>New BLogs</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button  type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save BLog Aticle</button>
                        <a  href="{{url('/admin_BLogs')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                              
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-12">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                      
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog Article Name</label>
                                <input class="form-control" type="text" name="blog_article_name" placeholder="BLog Name" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">BLog Article Pdf</label>
                                <input required="" class="form-control" type="file" name="blog_article_pdf" placeholder="BLog Image" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                         
                        <!--/ col -->
                        
                         <!-- col -->
                         
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Title</label>
                                <input required="" class="form-control" type="text" name="meta_title" placeholder="Meta Title" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Keywords</label>
                                <input required="" class="form-control" type="text" name="meta_keywords" placeholder="Meta Keywords" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Meta Description</label>
                                <input required="" class="form-control" type="text" name="meta_description" placeholder="Meta Description" id="example-text-input">
                            </div>
                         </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                                
                                <label for="example-text-input" class="col-form-label">Blog Description</label>
                                <div class="page-wrapper box-content">
                                <textarea  required="" class="form-control content" type="text" name="blog_article_desc" placeholder="Blog Description" id="example-text-input"></textarea>
                            </div>
                            </div>
                         </div>
                         <!-- col -->
                       
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
          
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
