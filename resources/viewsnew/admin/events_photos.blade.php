@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            
              
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">View {{  $event_details->event_name }}</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin-events')}}">Events</a></li>
                                <li><span>New {{  $event_details->event_name }}</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                   
                    <!--/ col -->      
                    
                   
                </div>
            </div>         
            <!-- container fluid -->
            <div class="container-fluid">
                <!-- tab -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                       <a class="nav-item nav-link" id="nav-home-tab"  href="{{ url('admin-events-view',['id'=>$event_details->id]) }}" role="tab" aria-controls="nav-home" aria-selected="true">View Blogs Details</a>
                        <a class="nav-item nav-link   " id="nav-profile-tab"  href="{{ url('event_videos',['id'=>$event_details->id])}}" role="tab" aria-controls="nav-profile" aria-selected="false">Videos</a>
                        <a class="nav-item nav-link active" id="nav-contact-tab"  href="{{ url('event-photos',['id'=>$event_details->id])}}" role="tab" aria-controls="nav-contact" aria-selected="false">Gallery</a>
                    </div>
                </nav>
                    <div class="tab-content" id="nav-tabContent">
                    <!-- tab 1-->
                    <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <!-- page title area start -->
            
                    </div>
                    <!-- tab 01-->
                    <!-- tabv 2-->
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                     
                    </div>
                    <!--/ tab 2-->
                    <!-- tab 03-->
                    <div class="tab-pane fade show active" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                       <div class="card">
    <div class="card-body">
       <div class="d-flex justify-content-between">
            <h4 class="header-title">List of Photos </h4>
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Photos </button>
       </div>
       @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="text-uppercase">
                        <tr>
                            <th scope="col">S.No:</th>
                           
                            <th scope="col">Images</th> 
                           
                            <th scope="col">Action</th> 

                           
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($photos as $value) 
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                           
                            <td><img src="{{ url('theme/uploads/photos').'/'.$value->img }}" alt="" class="img-fluid" height="100px" width="100px">{{ $value->img }}</td> 
                             
                                                               
                            <td>
                                <a href="{{ url('admin-photos-delete',['id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-danger mb-3" onclick="return confirm_delete();" ><i class="fa fa-plus-square"></i> Delete</button>
                                </a>
                            </td>                                        
                                                                   
                            
                           
                        </tr>      
                       @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
                  <!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <form  action = "{{ url('admin-photos-create',['id'=>Request::segment(2),'cat_id'=>'3']) }}"  enctype="multipart/form-data" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Photos </h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                  
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Photos</label>
                        <input required="" name="photo_img[]" placeholder="Photos " class="form-control" type="file"  value="" multiple="" id="example-text-input">
                    </div>
               
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </div>
        </form>
    </div>
    <!--/ new category modal -->
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
