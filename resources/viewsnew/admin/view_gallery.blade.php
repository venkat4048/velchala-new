@extends('admin.includes.styles')
@section('content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center py-3">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Gallery </h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/categories')}}">Home</a></li>
                    <li><span>Gallery </span></li>
                </ul>
            </div>
        </div>                    
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">

<!-- table card -->
<div class="card">
    <div class="card-body">
       <div class="d-flex justify-content-between">
            <h4 class="header-title">List of Gallery </h4>
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Gallery </button>
       </div>
       @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="text-uppercase">
                        <tr>
                            <th scope="col">S.No:</th>
                            <th scope="col"> Image</th> 
                            <th scope="col"> Image</th> 
                            <th scope="col">Action</th> 

                           
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($poems as $value) 
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td><img src="theme/gallery/{{ $value->poem_image }}" alt="" class="img-fluid" height="100px" width="100px"></td>                                        
                            <td>
                                <a href="{{ url('gallery-poem-delete',['id'=>$value->id]) }}">
                                  <button type="button" class="btn btn-danger mb-3" onclick="return confirm_delete();" ><i class="fa fa-plus-square"></i> Delete</button>
                                </a>
                            </td>                                        
                                                                   
                            
                           
                        </tr>      
                       @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--/ table -->
</div>
@endsection

   
    
  <!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <form  action = "{{url('/gallery-create')}}"  enctype="multipart/form-data" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Gallery Catergory</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                

                <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Gallery Image</label>
                        <input required="" name="gallery_img[]" placeholder="Gallery Image" class="form-control" type="file" multiple="" value="" id="example-text-input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </div>
        </form>
    </div>
    <!--/ new category modal -->
        <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
