@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/publications_store')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">New Publications</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_publications')}}">Publications</a></li>
                                <li><span>New Publications</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button name="submit" type="submit" class="btn btn-success mb-3" value="Save" id="btnSaveProduct">Save</button>
                        <button name="submit" type="submit" class="btn btn-success mb-3" value="Publish" id="btnSaveProduct">Publish</button>
                        <a  href="{{url('/admin_publications')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('Error Message')}}</strong>
        </div>
        @endif
            <!-- page title area end -->
            <div class="main-content-inner">
                             
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-8">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-form-label">Select Publisher</label>
                                <select required="" name="publishers_id" required="" class="form-control">
                                    <option>Select Publisher</option>
                                    @foreach($publishers as $value)
                                    <option value="{{ ucfirst($value->id) }}">{{ ucfirst($value->pub_name) }}</option>
                                    @endforeach     
                                </select>
                            </div>
                        </div>
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Publication Name</label>
                                <input required="" 
                                 class="form-control" type="text" name="publication_name"
                                
                                 placeholder="Publication Name" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Publication Price</label>
                                <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="publication_price" placeholder="Publication Price" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Discount Price</label>
                                <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="discount_price" placeholder="Discount Price" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                        
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Publication Stock</label>
                                <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="publication_stock" placeholder="Publication Stock" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Number of pages</label>
                                <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="publication_no_of_pages" placeholder="Number of pages" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          
                        <!--/ col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Author</label>
                                <input required="" class="form-control" type="text" name="author_name" placeholder="Author" id="example-text-input">
                            </div>
                         </div>
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                
                                <select required="" class="form-control" type="text" name="language" placeholder="Language" id="example-text-input">
                                <option value="">Select Langauge</option>
                                <option value="Telugu">Telugu</option>
                                <option value="English">English</option>
                                </select>

                            </div>
                         </div>
                        <!--/ col -->
                           <!-- col -->
                           <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Published Date</label>
                                <input class="form-control" type="date" name="published_date" placeholder="Published Date" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
                <div class="col-lg-4">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Describe</label>
                                        <textarea name="description" class="form-control" style="height:150px;" placeholder="Start Product Describe"></textarea>
                                    </div>
                                </div>
                                <!--/ col --> 
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" 
                                            name="publication_image" class="custom-file-input" id="inputGroupFile04">
                                            <label class="custom-file-label" for="inputGroupFile04">Choose Imaage</label>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <div class="custom-file">
                                           
                                        </div>
                                        
                                    </div>
                                </div>
                                    <div class="col-lg-12">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" 
                                            name="publication_pdf" class="custom-file-input" id="inputGroupFile04">
                                            <label class="custom-file-label" for="inputGroupFile04">Read Book</label>
                                        </div>
                                        
                                    </div>
                                </div>

                                <!--/ col -->
                                 <!-- col -->
                                
                                <!--/ col -->    
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
