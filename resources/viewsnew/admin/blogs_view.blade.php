@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
             
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">View {{  $blogs_details->blog_name }}</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/blogs')}}">Blogs</a></li>
                                <li><span>New {{  $blogs_details->blog_name }}</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        
                        <a  href="{{url('/admin_publications')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>         
            <!-- container fluid -->
            <div class="container-fluid">
                <!-- tab -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                       <a class="nav-item nav-link  active" id="nav-home-tab"  href="{{ url('blogs_view',['blogs_view'=>$blogs_details->blog_url]) }}" role="tab" aria-controls="nav-home" aria-selected="true">View Blogs Details</a>
                        <a class="nav-item nav-link" id="nav-profile-tab"  href="{{ url('blog_videos',['blogs_details'=>$blogs_details->id])}}" role="tab" aria-controls="nav-profile" aria-selected="false">Videos</a>
                        <a class="nav-item nav-link" id="nav-contact-tab"  href="{{ url('blog_photos',['blog_id'=>$blogs_details->id])}}" role="tab" aria-controls="nav-contact" aria-selected="false">Gallery</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <!-- tab 1-->
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/publications_store')}}" method = "post">
                        
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-8">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->  

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-form-label">Blog Name:</label>
                                {{  $blogs_details->blog_name }}
                            </div>
                        </div>
                        <!--/ col -->   
                         <!-- col -->
                        
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Meta Tiltle :</label>
                                 {{  $blogs_details->meta_title }}
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Meta Keywords :</label>
                                 {{  $blogs_details->meta_keywords }}
                            </div>
                         </div>
                        <!--/ col -->
                        
                         <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Meta Description :</label>
                                 {{  $blogs_details->meta_description }}
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                       
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Description :</label>
                                 {{  $blogs_details->blog_desc }}
                            </div>
                         </div>
                        
                        <!--/ col -->
                           <!-- col -->
                           <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Published Date :</label>
                                {{  date("d-m-Y",strtotime($blogs_details->created_at)) }}
                            </div>
                         </div>
                          
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
                <div class="col-lg-4">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                       
                                <figure class="figure-detail">
                                    <img src="{{ url('theme/uploads/blogs').'/'.$blogs_details->blog_img }}" alt="" class="img-fluid">
                                </figure>
                                    </div>
                                </div>
                                  
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
            
           
          </form>

                    </div>
                    <!-- tab 01-->
                    <!-- tabv 2-->
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                     
    <!--/ new category modal -->
                    </div>
                    <!--/ tab 2-->
                    <!-- tab 03-->
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <h1>Tab 03 content </h1>
                    </div>
                    <!--/ tab 03-->
                </div>
                <!--/ tab -->
            </div>
            <!--/ container fluid -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
