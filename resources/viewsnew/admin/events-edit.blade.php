@extends('admin.includes.styles')
@section('content')
            <!-- page title area start -->
            <form enctype='multipart/form-data' action = "{{url('/event-update')}}" method = "post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">   
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Edit events</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/dashboard')}}">Home</a></li>
                                <li><a href="{{url('/admin_events')}}">events</a></li>
                                <li><span>Edit events</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->

                    <div class="col-lg-6 text-right">
                        <button  type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save events</button>
                        <a  href="{{url('/admin_events')}}" class="btn btn-success mb-3">Cancel</a>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                              
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-12">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                      
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Event Name</label>
                                <input class="form-control" type="text" name="event_name" placeholder="Event Name" id="example-text-input" value="{{$events->event_name}}">
                                <input type="hidden" name="id" value="{{$events->id}}">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Event Location</label>
                                <input  class="form-control" type="text" name="event_location" placeholder="Event Location" id="example-text-input" value="{{$events->event_location}}">
                              
                            </div>
                         </div>
                        
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Event Image</label>
                                <input  class="form-control" type="file" name="event_img" placeholder="Event Image" id="example-text-input" >
                                  <input type="hidden" name="old_img" value="{{$events->img}}">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Event Date</label>
                                <input required="" class="form-control" type="date" name="event_date" placeholder="Event Date" id="example-text-input" value="{{$events->event_date}}">
                            </div>
                         </div>
                        <!--/ col -->
                       
                          <div class="col-lg-12">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Blog Description</label>
                                <textarea required="" class="form-control" type="text" name="event_des" placeholder="Blog Description" id="example-text-input">{{$events->event_des}}</textarea>
                            </div>
                         </div>
                         <!-- col -->
                       
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
          
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
          </form>
    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
@endsection
