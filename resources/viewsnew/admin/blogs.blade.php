@extends('admin.includes.styles')
@section('content')
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Blogs</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><span>Blogs</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Blogs</h4>
                        <a href="{{url('/blogs-add')}}"  class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> New Blog</a>
                   </div>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Sno</th>
                                    <th>Blog Name</th>
                                    <th>Blog Date</th>
                                   
                                    
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blogs as $value)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                   
                                    <td><a href="{{ url('blog_view',['blog_id'=>$value->id]) }}">{{ucfirst($value->blog_name)}}</a></td>
                                   
                                    <td>{{  date("d-m-Y",strtotime($value->created_at)) }}</td>
                                    <td><a href="{{ url('blogs-edit',['id'=>$value->id]) }}"><i class="fa fa-edit"></i>Edit</a></td>
                                    <td><a href="{{ url('admin-blog-delete',['id'=>$value->id]) }}" onclick="return confirm_delete();"><i class="fa fa-trash"></i>Delete</a></td>
                                         
                                </tr>
                               @endforeach  
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
            <!--/ table data -->
            </div>
                   <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
           @endsection
