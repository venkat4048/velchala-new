<!-- col --> 
<!-- alert add to cart-->
<div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated" role="alert">
<strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $ajax_popup->publication_name }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <!-- row -->
                <div  id="result_publications_ajax_popup" class="row justify-content-around ">
                    <div class="col-lg-5 text-center">
                        <img src="{{ url('theme/uploads/publications').'/'.$ajax_popup->img }}" class="img-fluid" alt="">
                    </div>
                    <!--/col-->
                        <!-- col -->
                        <div class="col-lg-7">
                            <article class="book-pop-article">
                                <h6 class="h6 border-bottom d-flex justify-content-between">
                                    <span>Description</span>
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist" >
                                        <span class="icon-heart"></span>
                                    </a>
                                </h6>
                                <p class="pb-0 mb-0">{{ ucfirst($ajax_popup->description )}}</p>
                            </article>
                            <dl class="list-dl">
                                <dt>Price:</dt>
                                <dd>
                                    <p class="price">
                                        <span class="offer">{{ ucfirst($ajax_popup->dis_price )}}</span>
                                        <span class="oldprice">{{ ucfirst($ajax_popup->price )}}</span>
                                    </p>
                                </dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Author:</dt>
                                <dd>{{ ucfirst($ajax_popup->author )}}</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Publisher:</dt>
                                <dd>{{ ucfirst($ajax_popup->pub_name )}}</dd>                           
                            </dl>                      
                            <dl class="list-dl">
                                <dt>No of Pages:</dt>
                                <dd>{{ ucfirst($ajax_popup->pages )}}</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Language:</dt>
                                <dd>{{ ucfirst($ajax_popup->language )}}</dd>                           
                            </dl>

                            <!--buttons -->
                            <div class="pt-2">
                                <a target="__blank" href="{{ url('theme/uploads/publications_pdf').'/'.$ajax_popup->pdf_file }}" class="orange-btn" >Read Online</a>
                                <a href="#" data-publ-id="{{ $ajax_popup->public_id }}" id="addtokcart-icon" class="orange-btn">Add to Cart</a> 
                            </div>
                            <!--buttons -->
                        </div>
                        </div>
                <!--/ row -->
                </div> 
                        <!--/col-->
  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <script>
         $("#addtokcart-icon").click(function() {

             publicationId = $(this).attr("data-publ-id");
             cart_quanity = 1;
             data = 'publicationId='+publicationId+'&cart_quanity='+cart_quanity+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/add_to_cart_data_ajax',
               data:data,
               success:function(results) {
                  
                  
                   $('#alertAddtocart').show();
                  
               }
            });
         });

      </script>