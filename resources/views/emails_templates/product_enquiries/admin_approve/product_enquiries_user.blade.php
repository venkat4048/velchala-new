<!DOCTYPE html>
<html>

<head>
    <title>Thanks from Vdesiconnect</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <a href="https://www.vdesiconnect.com/"><img src="https://www.vdesiconnect.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h1 style="font-weight: normal; margin:0;">Hi <strong style="color:#0098eb">
                        {{ $emaildata->se_name }}
                    </strong>
                </h1>


                <!--                --><?php
                //                dump($emaildata->getProduct);
                //                ?>

                <p>Your availability OTP Is <b> {{ $emaildata->se_otp }}</b></p>

                <div>
                    <b>Product Name : </b>
                    <a href="{{route('productPage', ['category'=>$emaildata->getProduct->getCategory->category_alias,'product'=>$emaildata->getProduct->p_alias])}}">
                        {{ $emaildata->getProduct->p_name }}
                    </a>
                </div>

                <br />
                <br />
                <br />


                <a href="{{route('productPage', ['category'=>$emaildata->getProduct->getCategory->category_alias,'product'=>$emaildata->getProduct->p_alias])}}" style="padding: 10px; border: 1px solid #ccc; margin: 20px">
                    Shop now
                </a>


                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">
                   Click on Buy Now button, Enter Your OTP and Enjoy shopping.
                </h3>
            </td>
        </tr>


    </table>
</div>
</body>

</html>
