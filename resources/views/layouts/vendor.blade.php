<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vdesi Connect Vendor Portal</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('store.include.headerstyles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">

    @yield('header_styles')

</head>
<body>


@yield('content')



@include('store.include.footer')
@include('store.include.footer_scripts')

<!-- include summernote css/js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<script>
    $(function () {
        $('.ckeditor').summernote({
            placeholder: 'Enter Message..',
            tabsize: 2,

            height: 150,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    })



</script>

@yield('footer_scripts')


</body>
</html>