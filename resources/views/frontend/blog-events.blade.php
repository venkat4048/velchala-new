<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.blog_layout')

  @section('content')
    <!--main-->   
    <main class="blog-main">

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
              @if(count($blogs)>0)
              @foreach($blogs as $value)
            <div class="col-md-6 col-lg-4 wow animate__animated animate__fadeInDown">
                    <div class="card blogcard">
                        <a href="{{ url('blog-events-details',['url'=>$value->blog_url]) }}">
                            <img class="card-img-top img-fluid" src="{{ url('theme/uploads/blogs').'/'.$value->blog_image }}">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title ptregular">{{ucfirst($value->blog_title)}}</h5>
                            <p class="card-text pb-3">{{substr(strip_tags($value->blog_description),0,100)}}..</p>
                            <p>Hyderabad <span class="d-inline-block px-3 small pb-3">|</span>{{ date("d-M-Y",strtotime($value->created_at)) }}</p>
                            <a href="{{ url('blog-events-details',['url'=>$value->blog_url]) }}" class="btn orange-btn">Read More</a>
                        </div>
                    </div>
                </div>
                 @endforeach
                 @else 
           <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif
                   
            <!--/ col -->                  
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

     @stop
   
    </body>
</html>