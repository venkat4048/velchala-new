@extends('admin.includes.styles')
@section('content')
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Users</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Users</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->
             @if(session('success'))
        <div class="alert alert-warning alert-dismissible" id="error-alert">
         <strong style="color: green;">{{session('success')}}</strong>
        </div>
        @endif
            <div class="card">
                <div class="card-body">
                   <!--  <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Blogs</h4>
                        <a href="{{url('/blogs-add')}}"  class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> New Blog</a>
                   </div> -->
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Sno</th>
                                    <th>User Name</th>
                                    <th>User Image</th>
                                    <th>Email</th>
                                    
                                    <th>Phone</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $value)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ucfirst($value->name)}}</td>
                                    <td>
                                        <img src="theme/user_profile/{{$value->image_url}}" alt="" class="img-fluid" height="100px" width="100px">
                                    </td>
                                    <td>{{ucfirst($value->email)}}</td>
                                   
                                    <td>{{ucfirst($value->phone_no)}}</td>
                                  
                                </tr>
                               @endforeach  
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          

            <!--/ table data -->
            </div>
           @endsection
