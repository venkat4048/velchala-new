<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <main class="subpage-main">
        <!-- figure -->
        <img src="theme/img/video-bg.jpg" class="img-fluid vspimg" alt="">
        <!--/ figure -->

        <!-- top vsp description -->
        <div class="top-vsp">
            <!-- container -->
            <div class="container whitebox">
                <!-- row -->
                <div class="row py-5 wow animate__animated animate__fadeInDown">                  
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <article class="pl-md-5">
                            <p class="fblue"><i>"Art is to Discover a Flash Extraordinary  in an Object Ordinary"</i></p>
                        </article>
                    </div>
                    <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 border-left">
                            <h1 class="ptregular pl-3">Welcome to Vishwanatha Sahithya Peetam</h1>
                        </div>
                    <!--/ col -->   
                    <!-- col -->
                    <div class="col-lg-12 py-4">
                        <h5 class="ptregular h5 fblue text-center">
                        <i> It was established by SISTER NIVEDITA FOUNDATION in the year. 2004. It publishes a bi-lingual quarterly, namely "JAYANTHI", which was actually started by Vishwanatha himself.</i>
                        </h5>
                    </div>
                    <!--/ col -->                
                </div>
                <!--/ row -->
                
            </div>
            <!--/ container -->
        </div>
        <!--/ top vsp description -->

        <!-- orange section -->
        <div class="orangebg py-3 my-3">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center vspban wow animate__animated animate__fadeInDown">
                        <img src="theme/img/kavi_samraat.jpg" alt="">
                        <h2 class="h1 ptregular fwhite">A Vision and a Projection</h2>
                        <p class="fwhite">
                            <i>"I am convinced also of this, the heart that must rule humanity is replaced by the mind.  It is a bane"</i>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ orange section -->

        <div class="vspmaster">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-6">
                        <p>It also publishes literary , cultural and educational books and organises seminars and symposia on books and subjects of literature, education and culture. </p>
                        <p>It periodically brings out special issues of JAYANTHI on eminent literary, cultural and educational personalities irrespective of the schools of thought and ideologies to which they belong. </p>
                        <p>Its main objective is to promote Telugu language and literature and undertake translations to and from Telugu for the benefit of the non-Telugu knowing people and the Telugus who are out of touch with their mother tongue, and to encourage writers and translators to write and translate.</p>
                        <p>It brings out short versions of books written by eminent men.</p>
                        <p>Its JAYANTHI is available as E-journal, and also its books.</p>
                        <p>It mostly aims at the institutions and organisations for its subscriptions.</p>
                        <p>Its publication Vishwanathavari "Muddu Vaddanlu"has been acclaimed as the best, and also the present publication VISHWANATHA -A LITERARY LEGEND.The special issues of its JAYANTHI are also very popular with the readers.</p>
                        <p>The Peetam is planning a series called "VISHWANATHA VAARI BANGARU BHAVAALU" on his basic thoughts on various matters of life and living.A book on Vishwanatha's adoption of words from other languages, and on why and how of it is also being planned as it is found that though he was a very great lover of Telugu,he respected other languages and borrowed from them freely when he felt that the borrowed words suited much more in certain contexts than the words from his own language.</p>
                        <p>There is a need to do a study on Vishwanatha's prose language, as contrary to many of his critics views his prose strikes verily as the day to day Telugu as is mostly used by the natives in their conversations. There is also to do research on his nudikaaraalu, samasalu, pratheekalu, deseeyaalu, saamethalu,palukuballu and varnanalu etc.</p>
                        <p> As it is sufficient to know everything about English language and literature if one carefully follows Shakespeare, it is sufficient to know everything about Telugu language and literature if one carefully follows Viswanatha.</p>
                        <p>Though Viswanatha Sahithya Peetam has great admiration for his sterling contribution to Telugu literature, we are not for highlighting Viswanatha for Viswanatha's sake, but for the sake of highlighting Telugu language and literature, as he is the very encyclopaedia, the very reference point to know all about them.</p>
                        <p>Take any genre of literature, be that be a story, a play, a novel, a padya, a geya, a song,a vyasa, a criticism, a multiple interpretation, an insight or a vision, even a translation, a preface, a speech. In everything he excelled, nay, he left his exclusive mark. It is for that reason that he becomes all inclusive to refer to in doubt.</p>
                        <p>And it is for that reason that this monumental volume is being brought out to give a birds eye view of his greatness to the readers to enable them to go back to his original works when they would like to know more about him and his writings. </p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">                       
                        <p>Viswanatha was more open minded and large hearted than many of his uncharitable critics who criticised and still criticise him on some other grounds than on the ground of literature. Let us not mix up the matters to mislead but be fair to him who was rare by reading and appropriately understanding him.</p>

                        <p>Literature should be loved and liked for its own sake but not for the sake of this or that? Then only it becomes a pleasure, a real treasure.Alas!That puristic spirit is dwindling and a compartmental approach to literature is increasing.with the result, a whole school of thought itself becomes an anathema to another.How then can good be literature produced in such an atmosphere and how can criticism be  wholesom and holistic?</p>

                        <p>Viswanatha is dubbed as the traditionalist by some. But, can there be modernity without tradition and tradition without modernity? Can there be higher step without the lower? New water in the river without the old? Son without the father and father without the grand father? What is the modern today becomes the traditional by tomorrow and what is the traditional today becomes the modern tomorrow  under the impact of the ever changing. Is the Marxism the same today? Democracy? Aristocracy? Monarchy? Where?</p>

                        <p>Viswanatha is dubbed as the “Sanatanist” without understanding the sense in which he used to use that word. For him , Santana is not old but that which is eternal and everlasting,the remaining the residual. Eternal values according to him do not get aged. Skills do, but not the basic values. By Sanatana, he means the eternal and ever lasting. According to him, all other things in tradition called the rites and rituals are matters which are ever changing according to the times and the climes. There is nothing stable and permanent about them. Fundamental values are common to all societies. Faith mainly relates to them. Rituals are like the formalities which differ and go on differing.Viswanathas traditionalism Should be understood from this point of view, of which he was quite clear.</p>

                        <p>Varna to him is environmental, ethnic and genetic. As the textures of soil differ so also the varnas or the colours and the characteristics of men too differ. He only confirmed the fact of life. Castes too remain forever but their names change and the hereditary system of the past changes with the expansion and decentralization of education. Here again he confirmed only the fact of life. He never defended the past system in the good old form.</p>

                        <p>This book clarifies many things said by him over which there is a lot of confusion and controversy more because of misinterpretation and mischievous interpretation.</p>


                        <p>Idealogies are more often than not is interpreted, more so the philosophies. And Viswanatha’s was a philosophy not an idealogy.</p>

                        <p>Idealogy deals with living, philosophy with life. And life is more complicated to say about than living, and more complex to give way to more than one interpretation. </p>
                       
                    </div>
                    <!--/ col -->
                    
                </div>
                <!--/ row -->
            </div>
            <!--/container -->
        </div>
    </main> 
    <!--/ main-->
   @stop
    </body>
</html>