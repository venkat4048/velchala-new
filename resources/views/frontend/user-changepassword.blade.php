<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
      @extends('includes.user_layout')

  @section('content')
     <!-- alert wishlist-->
     <div id="alertAddrtoCart" class="alert alert-success alert-dismissible" role="alert">
        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Cart List.  <a href="javascript:void(0)" class="d-block"><strong>View Cart Items</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <!--/ alert wishlist -->
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Change Password</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                       <li class="breadcrumb-item"><a href="{{url('user_profile_information')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Change Password</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                

             @stop
             @section('content2')
             
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 wow animate__animated animate__fadeInDown">

                                 @if(session('success'))
                                    
                                     <strong style="color: green;">{{session('success')}}</strong>
                                    
                                    @endif
                                    @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                               <form  enctype="multipart/form-data"       action ="{{url('/password-update')}}" method = "post">
                                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>"> 
                                  

                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="newPassword">New Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="newPassword" aria-describedby="emailHelp" placeholder="Enter New Password" name="new_psw">          
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="confirmNewPassword">Confirm New Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="confirmNewPassword" aria-describedby="emailHelp" placeholder="Confirm New Password" name="con_psw">          
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <div class="form-group">
                                         <button class="btn  orange-btn w-100">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
  @stop
   <script>
    $(document).ready(function(){        
        $('.addtoCart-icon').click(function(){
            $('#alertAddrtoCart').show();
        }) 
    });
   </script>    
   
    </body>
</html>