@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'events'=>'Events',
               ''=>'Add New'
               ),'Add New Events'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="events" action="{{ route('addNewEvents') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="event_name" type="text"
                                               placeholder="Title"
                                               name="event_name"
                                               value="{{ !empty(old('event_name')) ? old('event_name') : ((($event) && ($event->event_name)) ? $event->event_name : '') }}">
                                        @if ($errors->has('event_name'))
                                            <span class="text-danger help-block">{{ $errors->first('event_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Event Location</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="event_location" type="text"
                                               placeholder="Title"
                                               name="event_location"
                                               value="{{ !empty(old('event_location')) ? old('event_location') : ((($event) && ($event->event_location)) ? $event->event_location : '') }}">
                                        @if ($errors->has('event_location'))
                                            <span class="text-danger help-block">{{ $errors->first('event_location') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Event Date</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="event_date" type="date"
                                               placeholder="Title"
                                               name="event_date"
                                               value="{{ !empty(old('event_date')) ? old('event_date') : ((($event) && ($event->event_date)) ? $event->event_date : '') }}">
                                        @if ($errors->has('event_location'))
                                            <span class="text-danger help-block">{{ $errors->first('event_location') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control {{ (($event) && ($event->img)) ? '' : 'eventimage'}}"
                                               id="img" type="file" name="img"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('img'))
                                            <span class="text-danger">{{ $errors->first('img') }}</span>
                                        @endif

                                        @if(($event) && ($event->img))
                                            <div class="imagebox imagediv{{ $event->id }}">
                                                <img width="150" src="{{ url('theme/uploads/events/').'/'.$event->img }}"/>
                                                <a href="#" id="{{$event->id}}"
                                                   class="delete_event_image btn btn-danger btn-xs"
                                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea required name="event_des" id="event_des" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{{ !empty(old('event_des')) ? old('event_des') : ((($event) && ($event->event_des)) ? $event->event_des : '') }}</textarea>
                                        @if ($errors->has('event_des'))
                                            <span class="text-danger help-block">{{ $errors->first('event_des') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="events_status" required id="events_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('events_status')) && old('events_status')==$ks)  ? 'selected' : ((($event) && ($event->event_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('events_status'))
                                            <span class="text-danger help-block">{{ $errors->first('events_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_title" type="text"
                                               placeholder="Title"
                                               name="meta_title"
                                               value="{{ !empty(old('meta_title')) ? old('meta_title') : ((($event) && ($event->meta_title)) ? $event->meta_title : '') }}">
                                        @if ($errors->has('meta_title'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_title') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Keywords</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_keywords" type="text"
                                               placeholder="Title"
                                               name="meta_keywords"
                                               value="{{ !empty(old('meta_keywords')) ? old('meta_keywords') : ((($event) && ($event->meta_keywords)) ? $event->meta_keywords : '') }}">
                                        @if ($errors->has('meta_keywords'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_keywords') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_description" type="text"
                                               placeholder="Title"
                                               name="meta_description"
                                               value="{{ !empty(old('meta_description')) ? old('meta_description') : ((($event) && ($event->meta_description)) ? $event->meta_description : '') }}">
                                        @if ($errors->has('meta_description'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.events.script')

@endsection