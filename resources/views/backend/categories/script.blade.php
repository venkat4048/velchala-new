<script>


    $(function () {

        $('.titleCreateAlias').on('input', function () {
            // var aliasbox = $(this).data('aliasbox');
            var webseries_name = $(this).val();
            var alias = webseries_name.trim().replace(/[^a-zA-Z0-9]/g, '-').replace(/-{2,}/g, '-').toLowerCase();
            $('#category_alias').val(alias);
        });


        $('#category_parent_id').on('change', function () {
            var catvalue = $(this).val();
            if (catvalue == 0) {
                console.log("sdgsdgsdg");
                $('.categoryoptions').hide();
                $('.category_options').val('');
                $('.categoryimage').show();
                categoryimageRules();
            } else {
                $('.categoryoptions').show();
                categoryOptions();
                $('.categoryimage').hide();
            }
        });
        //
        // $('.webseries_add_validation').each(function () {
        //     $(this).validate({
        //         ignore: [],
        //         errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        //         errorElement: 'div',
        //         errorPlacement: function (error, e) {
        //             e.parents('.form-group > div').append(error);
        //         },
        //         highlight: function (e) {
        //             $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        //             $(e).closest('.help-block').remove();
        //         },
        //         success: function (e) {
        //             // You can use the following if you would like to highlight with green color the input after successful validation!
        //             e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
        //             e.closest('.help-block').remove();
        //         },
        //         rules: {
        //             w_title: {
        //                 required: true,
        //                 minlength: 2,
        //                 maxlength: 99,
        //             },
        //             w_alias: {
        //                 required: true,
        //                 remote: function (element) {
        //                     return {
        //                         url: '/admin/checkwebseriesAlias',
        //                         headers: {
        //                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //                         },
        //                         type: 'POST',
        //                         data: {
        //                             current_webseries_alias: function (form, event) {
        //                                 var formID = $(element).closest('form').attr('id');
        //                                 var currentAlias = $('#' + formID).find('.edit_current_webseries_alias');
        //                                 return $(currentAlias).val();
        //                             },
        //                             type: function (form, event) {
        //                                 return 'edit';
        //                             }
        //                         }
        //                     }
        //                 }
        //             },
        //         },
        //         messages: {
        //             w_title : {
        //                 required: 'Webseries title is required',
        //                 minlength: 'Your Category name must consist of at least {0} characters',
        //                 maxlength: 'Your Category name must consist of at least {0} characters'
        //             },
        //             w_alias : {
        //                 required: 'Alias is required',
        //                 remote: $.validator.format("This alias is already exist")
        //             },
        //         },
        //     });
        //
        // });


        $('.categoriesValid').each(function () {
            $(this).validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    category_parent_id: {
                        required: true
                    },
                    category_name: {
                        required: true
                    },
                    category_alias: {
                        required: true,
                        remote: function (element) {
                            return {
                                url: '/admin/ajax/checkNewCategoryAlias',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'POST',
                                data: {
                                    current_category_alias: function (form, event) {
                                        var formID = $(element).closest('form').attr('id');
                                        var currentAlias = $('#' + formID).find('.edit_current_category_alias');
                                        return $(currentAlias).val();
                                    },
                                    type: function (form, event) {
                                        return 'edit';
                                    }
                                }
                            }
                        }
                    },
                    category_status: {
                        required: true
                    }
                },
                messages: {
                    category_parent_id: {
                        required: 'Please Select Category (is this Parent Category or SubCategory)'
                    },
                    category_name: {
                        required: 'Please Enter Category Name'
                    },
                    category_alias: {
                        required: 'Alias is required',
                        remote: $.validator.format("This alias is already exist")
                    },
                    category_status: {
                        required: 'Please select status'
                    }
                },
            });

        });


        function categoryimageRules() {
            $(".categoryimage").rules("add", {
                required: true,
                accept: 'jpg,png,jpeg',
                messages: {
                    required: "Upload Poster Image",
                    accept: "Please Upload jpg,png,jpeg file formats only",
                }
            });
        }

        function categoryOptions() {
            $(".category_options").rules("add", {
                required: true,
                messages: {
                    required: "Required Category Options"
                }
            });
        }


    });


</script>