@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blog_news'=>'Blogs News',
               ''=>'Add New'
               ),'Add New Blog News'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogs_news" action="{{ route('addNewBlogNews') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="blog_news_name" type="text"
                                               placeholder="Title"
                                               name="blog_news_name"
                                               value="{{ !empty(old('blog_news_name')) ? old('blog_news_name') : ((($blognews) && ($blognews->blog_news_name)) ? $blognews->blog_news_name : '') }}">
                                        @if ($errors->has('blog_news_name'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_news_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Blog News Paper Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="blog_news_paper_name" type="text"
                                               placeholder="Blog News Paper Name"
                                               name="blog_news_paper_name"
                                               value="{{ !empty(old('blog_news_paper_name')) ? old('blog_news_paper_name') : ((($blognews) && ($blognews->blog_news_paper_name)) ? $blognews->blog_news_paper_name : '') }}">
                                        @if ($errors->has('blog_news_paper_name'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_news_paper_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Blog News Paper URL</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="blog_news_url" type="text"
                                               placeholder="Blog News Paper URL"
                                               name="blog_news_url"
                                               value="{{ !empty(old('blog_news_url')) ? old('blog_news_url') : ((($blognews) && ($blognews->blog_news_url)) ? $blognews->blog_news_url : '') }}">
                                        @if ($errors->has('blog_news_url'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_news_url') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Blog News Date</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required  class="form-control" id="blog_news_date" type="date"
                                               placeholder="Blog News Date"
                                               name="blog_news_date"
                                               value="{{ !empty(old('blog_news_date')) ? old('blog_news_date') : ((($blognews) && ($blognews->blog_news_date)) ? $blognews->blog_news_date : '') }}">
                                        @if ($errors->has('blog_news_date'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_news_date') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($blognews) && ($blognews->blog_news_img)) ? '' : 'blog_news_img'}}"
                                               id="blogimage" type="file" name="blog_news_img"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('blog_news_img'))
                                            <span class="text-danger">{{ $errors->first('blog_news_img') }}</span>
                                        @endif

                                        @if(($blognews) && ($blognews->blog_news_img))
                                            <div class="imagebox imagediv{{ $blognews->id }}">
                                                <img width="150" src="{{ url('theme/uploads/blog_news/').'/'.$blognews->blog_news_img }}"/>
                                                
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea required name="blog_news_des" id="blog_news_des" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{{ !empty(old('blog_news_des')) ? old('blog_news_des') : ((($blognews) && ($blognews->blog_news_des)) ? $blognews->blog_news_des : '') }}</textarea>
                                        @if ($errors->has('blog_news_des'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_news_des') }}</span>
                                        @endif
                                    </div>
                                </div>
                          
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blog_news.script')

@endsection