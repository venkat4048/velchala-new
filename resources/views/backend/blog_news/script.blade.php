<script>

    $(function () {
        $('.delete_blog_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/deleteblogimage',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                        $('#blogimage').addClass('blogimage');
                        imageRules();
                    }
                });
            }
        });
        $('#blogs').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                blog_title: {
                    required: true
                },
                blog_description: {
                    required: true
                },
                blog_status: {
                    required: true
                }
            },
            messages: {
                blog_title: {
                    required: 'Please enter title'
                },
                blog_description: {
                    required: 'Please enter description'
                },
                blog_status: {
                    required: 'Please select status'
                }
            },
        });
        imageRules();
    });

    function imageRules() {
        $(".blogimage").rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });
    }
</script>