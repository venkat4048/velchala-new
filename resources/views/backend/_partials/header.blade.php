<!-- navbar-->
<header class="header">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i
                                class="icon-bars"> </i></a><a href="{{route('dashboard')}}" class="navbar-brand">
                        <div class="brand-text d-none d-md-inline-block"><strong
                                    class="text-primary">Dashboard</strong></div>
                    </a></div>


                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Notifications dropdown-->

                    <li class="nav-item ">
                        <a id="notifications" href="{{route('admin_settings')}}"
                           class="nav-link">

                            Settings

                        </a>
                    </li>


                    <li class="nav-item dropdown" style="display: none">
                        <a id="notifications" rel="nofollow" data-target="#" href="#"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" class="nav-link"><i
                                    class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
                        <ul aria-labelledby="notifications" class="dropdown-menu">
                            <li><a rel="nofollow" href="#" class="dropdown-item">
                                    <div class="notification d-flex justify-content-between">
                                        <div class="notification-content"><i class="fa fa-envelope"></i>You have 6
                                            new messages
                                        </div>
                                        <div class="notification-time">
                                            <small>4 minutes ago</small>
                                        </div>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item">
                                    <div class="notification d-flex justify-content-between">
                                        <div class="notification-content"><i class="fa fa-twitter"></i>You have 2
                                            followers
                                        </div>
                                        <div class="notification-time">
                                            <small>4 minutes ago</small>
                                        </div>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item">
                                    <div class="notification d-flex justify-content-between">
                                        <div class="notification-content"><i class="fa fa-upload"></i>Server
                                            Rebooted
                                        </div>
                                        <div class="notification-time">
                                            <small>4 minutes ago</small>
                                        </div>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item">
                                    <div class="notification d-flex justify-content-between">
                                        <div class="notification-content"><i class="fa fa-twitter"></i>You have 2
                                            followers
                                        </div>
                                        <div class="notification-time">
                                            <small>10 minutes ago</small>
                                        </div>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">
                                    <strong> <i class="fa fa-bell"></i>view all notifications </strong></a></li>
                        </ul>
                    </li>
                    <!-- Messages dropdown-->
                    <li class="nav-item dropdown" style="display: none">
                        <a id="messages" rel="nofollow" data-target="#" href="#"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" class="nav-link">
                            <i class="fa fa-envelope"></i>
                            <span class="badge badge-info">10</span>
                        </a>
                        <ul aria-labelledby="notifications" class="dropdown-menu">
                            <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                    <div class="msg-profile"><img src="/admin/images/avatar-1.jpg" alt="..."
                                                                  class="img-fluid rounded-circle"></div>
                                    <div class="msg-body">
                                        <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span>
                                        <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                    <div class="msg-profile"><img src="/admin/images/avatar-2.jpg" alt="..."
                                                                  class="img-fluid rounded-circle"></div>
                                    <div class="msg-body">
                                        <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span>
                                        <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                                    <div class="msg-profile"><img src="/admin/images/avatar-3.jpg" alt="..."
                                                                  class="img-fluid rounded-circle"></div>
                                    <div class="msg-body">
                                        <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span>
                                        <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">
                                    <strong> <i class="fa fa-envelope"></i>Read all messages </strong></a></li>
                        </ul>
                    </li>


                    <!-- Languages dropdown    -->
                    <li class="nav-item dropdown">
                        <a id="languages" rel="nofollow" data-target="#" href="#"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"
                           class="nav-link language dropdown-toggle">
                            {!! Auth::guard('admin')->user()->name !!}
                        </a>
                        <ul aria-labelledby="languages" class="dropdown-menu">
                            {{--<li>
                                <a rel="nofollow" href="#" class="dropdown-item">
                                    <span>Settings</span>
                                </a>
                            </li>--}}
                            <li>
                                <a rel="nofollow" href="{{route('profile')}}" class="dropdown-item">
                                    <span>Profile</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('logout') }}" style="color: #1b1e21"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span class="d-none d-sm-inline-block">Logout</span>
                                    <i class="fa fa-sign-out"></i>

                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</header>