@extends('backend.layout')


@section('content')




    @if ($product->p_id!='')
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_books')}}">Books</a>
            </li>
            @if(isset($product->getCategory->pub_name))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/products?search=search&product_name=&search_category='.$product->getCategory->category_id.'&search_vendor=&p_status=')}}">{{$product->getCategory->category_name}}</a>
                </li>
            @endif
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">{{$product->p_name}}</a>
            </li>


        </ul>
    @else
        {!! getBreadcrumbs(
                     array(
                     'dashboard'=>'Home',
                     'admin_books'=>'Books',
                     ''=>'View'
                     ),'View Books'
                  ) !!}
    @endif




    <!-- dashboard main -->


        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                       <div class="row pt-3">
                           <div class="col-lg-6">
                               <h2 class="">{{$product->publication_name}}</h2>
                           </div>
                           <div class="col-lg-6">
                               <input type="button" value="Edit Product" class="btn btn-success"
                                      onclick="window.location.href='{{route('addNewProducts',['id'=>$product->b_id])}}'">
                               <input type="button" value="Back to listing" class="btn btn-success"
                                      onclick="window.location.href='{{route('admin_books')}}'">
                           </div>
                       </div>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->

                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row">
                            <!-- col left -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Book Details</h5>
                                        <!-- row -->

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Name</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{{$product->publication_name}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Overview</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{!! $product->description !!}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Image</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <!-- row image gallery -->
                                                <div class="row images-listview">
                                                   
                                                    <div class="col-md-2 px-1 position-relative imagediv_{{ $product->b_id }}">
                                                        <a data-fancybox="gallery" href="{{url('theme/uploads/publications').'/'.$product->img}}">
                                                            <img class="img-fluid imagecover position-relative"
                                                                    height="150"
                                                                    src="{{url('theme/uploads/publications').'/'.$product->img}}"/>
                                                                </a>
                                                            </div>
                                                        
                                                </div>


                                                <!--/ row iamge gallery -->
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                        
                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p> Book</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <!-- row image gallery -->
                                                <div class="row ">
                                                   
                                                <div class="col-md-2 px-1">

                                                    <a target="__blank" href="{{url('theme/uploads/publications').'/'.$product->pdf_file}}">
                                                    View Book 
                                                    </a>
                                                </div>
                                                        
                                                </div>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                </div>

                               
                                
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Videos</h5>

                                        <table class="table table-breakpoint">
                                            <thead>
                                            <tr>
                                                
                                                <th>Video Name</th>
                                                <th>Video</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                              @if(count($product_videos)>0)
                                              @foreach($product_videos as $val)
                                                        <tr>
                                                            <td> <iframe width="200" height="200" src="{{$val->video_url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                                                            <td>{{ $val->poem_name }}</td>
                                                           

                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Videos Found</td>

                                                    </tr>
                                                @endif
                                           
                                           
                                            </tbody>

                                        </table>


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->

                            </div>
                            <!--/ col left -->
                            <!-- col right -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Book primary Details</h5>

                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Sku Code</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  $product->sku_code }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Price</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  $product->price }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Publish Date</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  date("d-m-Y",strtotime($product->published_date)) }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Dis Price</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  $product->dis_price }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book pages</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  $product->pages }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Book Author</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{  $product->author }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Availability</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ ($product->stock=='in_stock') ? 'In Stock':'Out of Stock'}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                         <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Home Page</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p> 
                                                @if($product->home_page==1)
                                                Yes
                                                @else
                                                No
                                                @endif</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Status</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p> 
                                                @if($product->publications_status==1)
                                                Yes
                                                @else
                                                No
                                                @endif</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                    



                                       


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->
                            </div>



                            <!--/ col right -->


                        </div>
                        <!--/ row -->
                        <div class="row">
                            <!-- col right -->
                            <div class="col-md-6">

                            </div>
                            <!--/ col right -->
                        </div>


                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->

    <!--/ dashboard main -->

@endsection



