{!! javascriptFunctions() !!}

<script>

       


    var i = 1;
        <?php
        if($product_videos){ ?>
    var i = {{ count($product_videos) }}
            ++i;

    <?php  } ?>


    

    $("#add").click(function () {

        ++i;

        $("#dynamicTable").append('<tr>' +
            
            '<td class="form-group">' +
            '<input required type="text" name="book_videos[]" placeholder="Ex : https://www.youtube.com/embed/dWFvZl3D3us" class="form-control sku_option" />' +
            '</td>' +

            '<td class="form-group">' +
            '<input required type="text" name="book_videos_name[]" placeholder="Ex : Interviews" class="form-control sku_option" />' +
            '</td>' +
            
            '<td>' +
            '<button type="button" class="btn btn-danger remove-tr">Remove</button>' +
            '</td>' +
            '</tr>');

        validateSKU()
    });

    $(document).on('click', '.remove-tr', function () {

        var confirmMsg = confirm("Are you sure want to remove?");
        if (confirmMsg == true) {

            $(this).parents('tr').remove();
            var id = $(this).closest('tr').prop('id');
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('seller_remove_options') }}',
                    type: 'POST',
                    data: {'id': id},
                    success: function (response) {
                    }
                });
            }


        } else {
            // txt = "You pressed Cancel!";
        }


    });


    function catOptions(catID) {
        if (catID == 1) {  //cake
            $('.itemspecial').html('Flavour');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#cake').show();
            $('#cake').find(":input").attr("disabled", false);


        } else if (catID == 2) {  //flowers
            $('.itemspecial').html('Occation');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#flowers').show();
            $('#flowers').find(":input").attr("disabled", false);


        } else if (catID == 3) { //Gifts
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#gifts').show();
            $('#gifts').find(":input").attr("disabled", false);

        } else if (catID == 4) { // chacolates
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#chocolates').show();
            $('#chocolates').find(":input").attr("disabled", false);
        } else if (catID == 5) { // jweleey
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#jewllary').show();
            $('#jewllary').find(":input").attr("disabled", false);

        } else if (catID == 6) {  // millets
            $('.itemspecial').html('Product Name');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#millets').show();
            $('#millets').find(":input").attr("disabled", false);
        }

    }


    $(function () {

        $('.discount_type, .p_vdc_vendor_discount, .p_regular_price').on("input", function () {

            discountCalc('.p_regular_price', '.discount_type', '.p_vdc_vendor_discount', '.p_vdc_price_vendor')
        });


        $('.delete_product_image').on('click', function () {
            console.log("sdgasdgasdgasdgsad");
            var image = $(this).attr('id');
            var token = $(this).data('token');
            console.log(image);
            console.log(token);
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{route('VendordeleteProductimage')}}',
                    type: 'POST',
                    data: {'imageid': image, _token: token},
                    success: function (response) {
                        $('.imagediv_' + image).remove();
                    }
                });
            }
        });

        $('.titleCreateAlias').on('input', function () {
            // var aliasbox = $(this).data('aliasbox');
            var product_name = $(this).val();
            var alias = product_name.trim().replace(/[^a-zA-Z0-9]/g, '-').replace(/-{2,}/g, '-').toLowerCase();
            $('#p_alias').val(alias);
        });


        // $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();

        var ExistCatId = $('#p_cat_id').val();

        if (ExistCatId) {
            catOptions(ExistCatId);
        }

        $('#p_cat_id').on('change', function () {
            // alert("testest");

            // console.log('asd');

            var catID = $(this).val();
            if (catID) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{route('showCategoryTypes')}}',
                    type: "POST",
                    data: {'catID': catID},
                    dataType: "json",
                    success: function (data) {

                        var Types = data.types;
                        var Others = data.others;


                        $('#p_sub_cat_id').empty();
                        $('#p_sub_cat_id').append('<option value="">--Select SubCategory--</option>');
                        $.each(Types, function (key, value) {
                            if ($('.p_sub_cat_id').val() == key) {
                                $('#p_sub_cat_id').append('<option value="' + key + '" selected>' + value + '</option>');
                            } else {
                                $('#p_sub_cat_id').append('<option value="' + key + '">' + value + '</option>');
                            }
                        });


                        $('#p_item_spl').empty();
                        $('#p_item_spl').append('<option value="">--Select Special--</option>');
                        $.each(Others, function (key, value) {
                            if ($('.p_item_spl').val() == key) {
                                $('#p_item_spl').append('<option value="' + key + '" selected>' + value + '</option>');
                            }else{
                                $('#p_item_spl').append('<option value="' + key + '">' + value + '</option>');
                            }
                        });


                    }
                });

                catOptions(catID)

            } else {
                $('#p_sub_cat_id').empty();
                $('#p_item_spl').empty();
            }
        }).change();


    })
</script>


<script>
    jQuery(document).ready(function () {


        $('.delete_productExtra_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/deleteproductextraimage',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                        $('#productExtraimage' + image).addClass('productExtraimage');
                        imageRules();
                    }
                });
            }
        });

        imageRules();


    });

    function imageRulesForProducts() {
        $(".productimage").rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });

    }

    function imageRules() {
        $(".productExtraimage").each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Upload image to save"
                }
            });
        })


    }

</script>