@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Users'
              ),'Users'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Users</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">DOB</th>
                                    <th scope="col">gender</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($users)>0)
                                    @foreach($users as $item)
                                        <tr>
                                            <th scope="row">{{ $item->order_id }}</th>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                @if(!empty($item->image))

                                                    <img width="80" src="{{ '/uploads/users/thumbs/'.$item->image }}"/>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ ($item->mobile) ? $item->mobile : '-' }}</td>
                                            <td>{{ ($item->dob) ? \Carbon\Carbon::parse($item->dob)->format('d/m/Y') : '-' }}</td>
                                            <td>{{ ($item->gender) ? $item->gender : '-' }}</td>
                                            <td>{{ allStatuses('general',$item->status) }}</td>
                                            <td>
                                                <div class="">

                                                        <a class="dropdown-item"
                                                           href="{{ route('userAddress',['id'=>$item->id]) }}"><i
                                                                    class="fa fa-eye"></i> View</a>



                                                    <form method="POST" id="users_form_{{ $item->id }}"
                                                          action="{{ route('users') }}"
                                                          accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                          style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id"
                                                               value="{{ $item->id }}"/>
                                                        <button type="submit" class="dropdown-item"
                                                                title="Delete User"
                                                                onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </button>

                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $users->firstItem() }}
                        to {{ $users->lastItem() }} of {{ $users->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $users->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection