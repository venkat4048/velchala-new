@extends('backend.layout')
@section('title', $title)

<?php
//dd('asd');
?>

@section('headerStyles')


    <style type="text/css">


        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 300;
            font-size: 13px;
            margin: 0;
            padding: 0;
        }

        body a {
            text-decoration: none;
            color: inherit;
        }

        body a:hover {
            color: inherit;
            opacity: 0.7;
        }

        body .container {
            /*width: 900px;*/
            margin: 0 auto;
            padding: 0 20px;
        }

        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        body .left {
            float: left;
        }

        body .right {
            float: right;
        }

        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        body .no-break {
            page-break-inside: avoid;
        }

        header {
            margin-top: 20px;
            margin-bottom: 50px;
        }

        header figure {
            float: left;
            width: 60px;
            height: 60px;
            margin-right: 15px;
            text-align: center;
        }

        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }

        header .company-address .title {
            color: #8BC34A;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }

        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #8BC34A;
            color: white;
        }

        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }

        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }

        header .company-contact .circle img {
            vertical-align: middle;
        }

        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }

        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        .date {
            line-height: 20px;
        }

        section .details {
            margin-bottom: 55px;
        }

        section .details .client {
            width: 50%;
            line-height: 20px;
        }

        section .details .client .name {
            color: #8BC34A;
        }

        section .details .data {
            width: 50%;
            text-align: right;
        }

        section .details .title {
            margin-bottom: 15px;
            color: #8BC34A;
            font-size: 3em;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;

        }

        section table .qty, section table .unit, section table .total {
            width: 15%;
        }

        section table .desc {
            width: 55%;
        }

        section table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        section table thead th {
            padding: 5px 10px;
            background: #8BC34A;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table thead th:last-child {
            border-right: none;
        }

        section table thead .desc {
            text-align: left;
        }

        section table thead .qty {
            text-align: center;
        }

        section table tbody td {
            padding: 10px;
            background: #E8F3DB;
            color: #000;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }

        section table tbody td:last-child {
            border-right: none;
        }

        section table tbody h3 {
            margin-bottom: 5px;
            color: #8BC34A;
            font-weight: 600;
            font-size: 15px;
        }

        section table tbody .desc {
            text-align: left;
            line-height: 20px;
            font-size: 13px;
        }

        section table tbody .qty {
            text-align: center;
        }

        section table.grand-total {
            margin-bottom: 45px;
        }

        section table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
            line-height: 25px;
        }

        section table.grand-total .desc {
            background-color: transparent;
        }

        section table.grand-total tr:last-child td {
            font-weight: 600;
            color: #8BC34A;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }

        footer .thanks {
            margin-bottom: 40px;
            color: #8BC34A;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }

        footer .notice {
            margin-bottom: 25px;
            line-height: 22px;
        }

        footer .end {
            padding-top: 5px;
            border-top: 2px solid #8BC34A;
            text-align: center;
        }

    </style>

    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet"/>
    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet"/>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'users'=>'Users',
              ''=>'User Address'
              ),'User Address'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


              <div class="container">

                  <div class="col-md-12">

                      <?php
                      $order_currency = $orders->order_currency;
                      ?>




                      @if (Session::has('flash_message'))
                          <br/>
                          <div class="alert alert-success alert-dismissable">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>{{ Session::get('flash_message' ) }}</strong>
                          </div>
                      @endif
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title">Order Details</strong>

                              <form method="post" id="editServices" style="display: none"
                                    action="{{ route('orderStatusUpdate',['id'=>$orders->order_id]) }}"
                                    class="form-horizontal editServices"
                                    autocomplete="off"
                                    enctype="multipart/form-data">
                                  {{ csrf_field() }}


                                  <input type="hidden" name="order_id"
                                         value="{{$orders->order_id}}"/>

                                  <div class="row">

                                      <div class="col-4  ">

                                          <div class="form-group">
                                              <label for="hf-email"
                                                     class=" form-control-label">Status Update</label>
                                              <select class="form-control"
                                                      id="order_status"
                                                      type="text"
                                                      placeholder="Contact Person"
                                                      value=""
                                                      name="order_status">
                                                  <option value="">--Select Status</option>
                                                  <option value="Paid" {{ isset($orders) && ($orders->order_status == 'Paid') ? 'selected' : ''}}>
                                                      Paid
                                                  </option>
                                                  <option value="Shipped" {{ isset($orders) && ($orders->order_status == 'Shipped') ? 'selected' : ''}}>
                                                      Shipped
                                                  </option>
                                                  <option value="OutForDelivery" {{ isset($orders) && ($orders->order_status == 'OutForDelivery') ? 'selected' : ''}}>
                                                      Out for delivery
                                                  </option>
                                                  <option value="Delivered" {{ isset($orders) && ($orders->order_status == 'Delivered') ? 'selected' : ''}}>
                                                      Delivered
                                                  </option>
                                              </select>

                                          </div>
                                      </div>
                                      <input type="submit"
                                             class="btn btn-primary"
                                             value="Update"/>

                                  </div>


                              </form>

                              <div class="btn-group" role="group" aria-label="Basic example" style="float: right; display: none">
                                  <a href="#" class="btn btn-secondary {{ isset($orders) && ($orders->order_status ==
                                'Paid') ? 'active' : ''}}">Paid</a>
                                  <a href="#"
                                     class="btn btn-secondary {{ isset($orders) && ($orders->order_status == 'Shipped') ? 'active' : ''}}">Shipped</a>
                                  <a href="#"
                                     class="btn btn-secondary {{ isset($orders) && ($orders->order_status == 'OutForDelivery') ? 'active' : ''}}">Out
                                      for delivery</a>
                                  <a href="#"
                                     class="btn btn-secondary {{ isset($orders) && ($orders->order_status == 'Delivered') ? 'active' : ''}}">Delivered</a>
                              </div>

                          </div>
                          <div class="card-body table-responsive">


                          <?php
                          $orderDetails= $orders;
                          ?>

                          <?php
                          $order_currency = $orderDetails->order_currency;

                          //                            dump($orderDetails);
                          ?>

                          <!-- gray block -->
                              <div class="grayblock">
                                  <ul class="row primarydetails">
                                      <li class="col-lg-4">
                                          <h6>Order number </h6>
                                          <p>
                                              <a href="{{ route('orderDetails', ['id'=>$orderDetails->order_reference_number]) }}">
                                                  {{ $orderDetails->order_reference_number }}
                                              </a>
                                          </p>
                                      </li>
                                      <li class="col-lg-4">


                                          <h6>Order Date & Time </h6>
                                          <p>
                                              {{ \Carbon\Carbon::parse( $orderDetails->created_at)->format('d-m-Y h:m:s a') }}

                                          </p>
                                      </li>
                                      <li class="col-lg-4">
                                          <h6>Payment Info</h6>
                                          <p>
                                              Pay With
                                              <b>
                                                  {{ $orderDetails->order_payment_mode }}
                                              </b>

                                              <br/>

                                              {!! PayapPaymentStatusShow($orderDetails->order_payment_status) !!}

                                          </p>
                                      </li>
                                  </ul>


                                  <ul class="row primarydetails">
                                      <li class="col-lg-3">
                                          <h6>Products </h6>
                                          <p>
                                              Products Price :
                                              {!! currencySymbol($order_currency) !!}
                                              {{  $orderDetails->order_sub_total_price }}

                                          </p>
                                      </li>

                                      <li class="col-lg-3">
                                          <h6>Shipping Charges </h6>
                                          <span class="forange fbold">
                                                {!! currencySymbol($order_currency) !!}
                                              {{  $orderDetails->order_shipping_price }}

                                            </span>
                                      </li>


                                      <li class="col-lg-3">
                                          <h6>Coupon Discount</h6>
                                          <p>

                                              {{--                                                <br/>--}}
                                              {{--                                                Sub Total Price :--}}
                                              {{--                                                {!! currencySymbol($order_currency) !!}--}}
                                              {{--                                                {{  $orderDetails->order_sub_total_price }}--}}


                                              Coupon Price : {!! currencySymbol($order_currency) !!}
                                              {{  $orderDetails->order_coupon_discount_amount }}
                                              <br/>

                                              code: {{  $orderDetails->order_coupon_code }}
                                              <br/>
                                              Value: {{  $orderDetails->order_coupon_discount }}  {{  ($orderDetails->order_coupon_discount_type=='Percentage')?'%':$orderDetails->order_coupon_discount_type }}

                                          </p>
                                      </li>

                                      <li class="col-lg-3">
                                          <h6>Payble Amount </h6>
                                          <span class="forange fbold">
                                                {!! currencySymbol($order_currency) !!}
                                              {{  $orderDetails->order_total_price }}
                                            </span>
                                      </li>

                                  </ul>

                                  <?php
                                  // dump($orderDetails->orderItems);
                                  ?>

                              </div>
                              <!--/ gray block -->


                              @if (!empty($orderDetails->orderItems))
                                  @foreach($orderDetails->orderItems AS $orderItems)

                                      <?php
                                      if (isset($orderItems->getProduct->productImages[0])) {
                                          $item_img = url('/uploads/products/' . $orderItems->getProduct->productImages[0]->pi_image_name);

                                      } else {
                                          $item_img = '/frontend/images/no-image.png';
                                      }

                                      // dump($orderItems);
                                      ?>

                                      <div class="row py-2">
                                          <!-- col -->
                                          <div class="col-lg-2 col-md-2">
                                              <figure class="imgproduct">
                                                  <a href="javascript:void(0)">
                                                      <img src="{{$item_img}}" alt="" title="" class="img-fluid"></a>
                                              </figure>
                                          </div>
                                          <!--/ col -->
                                          <!-- col -->
                                          <div class="col-lg-6 col-md-5">
                                              <h6 class="pb-2">
                                                  {{ $orderItems->oitem_product_name  }}

                                              </h6>
                                              <p>
                                                  <b>Gift Message:</b>
                                                  {{ $orderItems->oitem_message  }}
                                              </p>

                                              <p>
                                                  <b>Name On The gift:</b>
                                                  {{ $orderItems->oitem_nameongift  }}
                                              </p>
                                              <p>
                                                  <b>Options:</b>
                                                  {{ sortProductOptions($orderItems->oitem_product_options)  }}
                                              </p>


                                              {{--<a href="javascript:void(0)" class="whitebtn">Paynow</a>--}}
                                              {{--<a href="javascript:void(0)" class="whitebtn">return / replace</a>--}}
                                          </div>
                                          <div class="col-lg-4 col-md-5">
                                              <div>
                                                  Price
                                                  : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_sub_total  }}
                                              </div>

                                              <div>

                                                  @if (isset($orderItems->oitem_delivery_date) && $orderItems->oitem_delivery_date != '')

                                                      <p>
                                                          <b>
                                                              User Request Delivery time.

                                                          </b> <br/>
                                                          {{ $orderItems->oitem_delivery_date }} /
                                                          {{ $orderItems->oitem_delivery_time }}

                                                          {{--                                                        {{  (isset($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                      </p>
                                                  @endif

                                              </div>

                                              <div>
                                                  Delevery Charge
                                                  : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_delivery_charge  }}
                                              </div>
                                              <div>
                                                  Total

                                                  <span class="forange fbold"> : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_sub_total +$orderItems->oitem_delivery_charge }} </span>

                                              </div>
                                          </div>
                                          <!--/ col -->


                                          @if($orderDetails->order_payment_status == 'approved')



                                              <?php

                                              switch ($orderDetails->order_status) {
//                                                case '':
//                                                    break;
//                                                case '':
//                                                    break;
                                                  default:
                                                      $status_bar = 50;
                                                      $status_bar_name = $orderDetails->order_status;
                                                      break;
                                              }
                                              ?>


                                              <?php
                                              //                                        dump($orderItems->oitem_status);
                                              ?>

                                              {{--                                                {{ $orderItems->oitem_status }}--}}
                                              {{--                                                {{ $orderDetails->order_status }}--}}



                                              {!! orderItemStatus($orderItems->oitem_status); !!}



                                          @endif
                                      </div>
                              @endforeach
                          @endif


                          <!-- row -->

                              <!--/ row -->
                              <div class="row">
                                  <div class="col-lg-6 col-md-6">
                                      <h6 class="h6 ">Status:
                                          <span class="fgreen">
                                                {!! OrderStatus($orderDetails->order_status) !!}
                                            </span>
                                      </h6>
                                  </div>
                                  {{--<div class="col-lg-6 col-md-6 text-right">--}}
                                  {{--<h6 class="h6 ">Delivered on:--}}
                                  {{--<span class="fgreen">11 July 2019</span>--}}
                                  {{--</h6>--}}
                                  {{--</div>--}}
                              </div>
                              <!-- order status -->
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="grayblock  p-2 tw100 ">
                                          <p class="fgreen fbold">Shipping Information</p>
                                          <p>
                                              <?php
                                              $address_info = unserialize($orderDetails->order_delivery_address);
                                              ?>
                                              {{  implode(', ', $address_info) }}
                                          </p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 text-left">

                                      <div class="grayblock  p-2 tw100 ">
                                          <p class="fgreen fbold">Billing Information</p>
                                          <p>
                                              <?php
                                              $address_billing_info = unserialize($orderDetails->order_billing_address);
                                              ?>
                                              {{  implode(', ', $address_billing_info) }}
                                          </p>
                                      </div>


                                  </div>
                              </div>






                          </div>
                      </div>

                  </div>

              </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection
